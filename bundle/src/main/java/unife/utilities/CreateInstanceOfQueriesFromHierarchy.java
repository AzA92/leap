/**
 * This file is part of LEAP.
 *
 * LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, but
 * some components can be used as stand-alone.
 *
 * LEAP is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * LEAP is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package unife.utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author riccardo
 */

class Node {
    String name;
    Node parent;
    List<Node> children;

    public Node (String name) {
        this.name = name;
        parent = null;
        children = new ArrayList<Node>();
    }
    
    public void add (Node child) {
        child.parent = this;
        children.add(child);
    }
    
    public List<Node> succ() {
        List<Node> succ = new ArrayList<Node>();
        for (Node son : children) {
            succ.addAll(son.succ());            
        }
        succ.addAll(children);
        return succ;
    }
    
    public List<Node> prec() {
        List<Node> prec = new ArrayList<Node>();
        if (parent.name.equals("owl:Thing"))
            return prec;
        else {
            prec.addAll(parent.prec());
            prec.add(parent);
            return prec;
        }
    }
    
}

public class CreateInstanceOfQueriesFromHierarchy {

    public static void main(String[] args) {

        try {

           


                String dir = "/home/riccardo/Desktop/creation_onto/dbp/";  //directory
                String file = "class_hierarchy.txt"; //nome file gerarchia classi
                String initCl = "http://dbpedia.org/ontology/"; //prefisso classi
                String file_onto = "onto.txt"; //file dove salva gli individui da mettere in ontologia
                String file_query = "query.txt"; //file dove salva le query
                int nInd = 10; // numero di individui

                // SWORE
//                String nc="swore";
//                String dir = "/home/riccardo/Desktop/test_celoe/swore/";
//                String file = "classes_swore";
//                String fileInd = "individuals_swore";
//                String file_out = nc+"_queries_exp.sh";
//                String initCl = "http://ns.softwiki.de/req/"; 
                int nQ = 100;

                File cid = new File(dir + file);
                BufferedReader input = new BufferedReader(new FileReader(cid));

                List<Node> map = new ArrayList<Node>();

                String text;
                int n_sp = 0, n_sp_new = 0;
                if ((text = input.readLine()).equals("")) {
                    text = input.readLine();
                }

                Node tree = new Node(text.trim());
                Node cN = tree;
                n_sp = 1;
                n_sp_new = 1;

                while ((text = input.readLine()) != null) {

                    if (!text.equals("")) {

                        n_sp_new = 0;
                        for (int i = 0; i < text.length(); i++) {
                            if (text.charAt(i) == ' ') {
                                n_sp_new++;
                            } else {
                                break;
                            }
                        }

                        if (n_sp_new > n_sp) {
                            Node newN = new Node(text.trim());
                            cN.add(newN);
                            map.add(newN);
                            n_sp = n_sp_new;
                            cN = newN;
                        } else if (n_sp == n_sp_new) {
                            Node newN = new Node(text.trim());
                            cN.parent.add(newN);
                            map.add(newN);
                            cN = newN;
                        } else if (n_sp > n_sp_new) {

                            n_sp_new--;
                            int step = (n_sp - n_sp_new) / 3;

                            for (int i = 0; i < step; i++) {
                                cN = cN.parent;
                            }

                            Node newN = new Node(text.trim());
                            cN.parent.add(newN);
                            map.add(newN);
                            n_sp = n_sp_new + 1;
                            cN = newN;
                        }
                    }
                }
                input.close();

                System.out.println("done!");

                PrintStream outo = null;
                outo = new PrintStream(new FileOutputStream(dir + file_onto));
                PrintStream outq = null;
                outq = new PrintStream(new FileOutputStream(dir + file_query));

                    //instance -> query con spiegazioni
                for (int i = 0; i < nInd; i++) {
                    boolean created = false;
                    while (!created) {

                        Node sel = map.get((int) (Math.random() * map.size()));
                        List<Node> p = sel.prec(); // in su
                        List<Node> s = sel.succ();
                        if (!p.isEmpty()) {
                            outq.println("\t<NamedIndividual rdf:about=\"" + i + "\">\n"
                                    + "\t        <rdf:type rdf:resource=\"" + initCl + p.get((int) Math.random() * p.size()).name + "\"/>\n"
                                    + "\t</NamedIndividual>");
                            if (!s.isEmpty()) {
                                outo.println("\t<NamedIndividual rdf:about=\"" + i + "\">\n"
                                        + "\t        <rdf:type rdf:resource=\"" + initCl + s.get((int) Math.random() * s.size()).name + "\"/>\n"
                                        + "\t</NamedIndividual>");
                            } else {
                                outo.println("\t<NamedIndividual rdf:about=\"" + i + "\">\n"
                                        + "\t        <rdf:type rdf:resource=\"" + initCl + sel.name + "\"/>\n"
                                        + "\t</NamedIndividual>");
                            }
                            created = true;
                        }

                    }

                }

                

            

        } catch (IOException ioException) {
            System.err.println(ioException);
        }

    }

}
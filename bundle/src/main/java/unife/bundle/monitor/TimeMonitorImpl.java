/**
 *  This file is part of LEAP.
 * 
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, 
 *  but some components can be used as stand-alone.
 * 
 *  LEAP is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  LEAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package unife.bundle.monitor;


/**
 *
 * @author Riccardo Zese <riccardo.zese@unife.it>, Giuseppe Cota <giuseta@gmail.com>
 */
public class TimeMonitorImpl implements TimeMonitor {
    
    private boolean stop = false;
    private int execTime;
    private Thread t;
    
    public TimeMonitorImpl() {
        execTime = 0;
    }
    
    @Override
    public void startMonitoring() {
        if (execTime > 0) {
            t = new Thread(new TimerChecker());
            t.start();
        }
    }
    
    @Override
    public void setParamAndStart(int time) {
        setExecTime(time);
        startMonitoring();
    }
    
    public void setExecTime(int time) {
        this.execTime = time * 1000;
    }
    
    public int getExecTime() {
        return execTime;
    }
    
    @Override
    public void stopMonitoring() {
        if (t != null && t.isAlive())
            t.stop();
    }

    @Override
    public boolean isCancelled() {
//            if (execTimer == null)
//                return false;
//            else
//                //System.out.println("time " + execTimer.getElapsed() + " " + maxTime);
//                return (execTimer.getElapsed() > maxTime);
        return stop;
    }
    
    private class TimerChecker implements Runnable
    {
        public void run() {
            try {
                Thread.sleep(getExecTime());
                stop = true;
                
            } catch (InterruptedException ex) {
                //Logger.getLogger(PelletExplain.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                //Logger.getLogger(PelletExplain.class.getName()).log(Level.SEVERE, null, ex);
            }
        
        }

}
}

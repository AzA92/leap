/**
 * This file is part of LEAP.
 *
 * LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, but
 * some components can be used as stand-alone.
 *
 * LEAP is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * LEAP is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package unife.edgeRemote;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.RemoteServer;
import java.rmi.server.ServerNotActiveException;
import java.rmi.server.UnicastRemoteObject;
import mpi.MPI;
import mpi.MPIException;
import org.apache.log4j.Logger;
import unife.bundle.logging.BundleLoggerFactory;
import unife.edge.EDGEStatImpl;
import unife.edge.mpi.MPIUtilities;
import unife.rmi.communication.Communication;

/**
 *
 * @author Riccardo Zese <riccardo.zese@unife.it>, Giuseppe Cota
 * <giuseta@gmail.com>
 */
public class EDGERemoteServerImpl implements EDGERemoteServer {

    //private static final String POLICY="src/bundle/edgeserver/server.policy";
    private static final Logger logger = Logger.getLogger(EDGERemoteServerImpl.class.getName(), new BundleLoggerFactory());
    // the creation of EDGERemote objects is limited to one
    private static boolean EDGEInstatiated = false;

    private static boolean parallel = false;

    // used for test
    public boolean registered = false;

    private String policyCodebase = EDGERemoteServerImpl.class.getProtectionDomain().getCodeSource().getLocation().toString();

    private final int port = 1099;

    private Registry registry = null;

    String pathPolicyFile = null;

    /**
     * @param argv the command line arguments
     * @throws java.net.MalformedURLException
     * @throws java.lang.InterruptedException
     * @throws java.rmi.RemoteException
     */
    public static void main(String[] argv) throws MalformedURLException, InterruptedException, RemoteException {
        int myRank = MPIUtilities.MASTER;
        try {
            argv = MPI.Init(argv);
            myRank = MPI.COMM_WORLD.getRank();
            parallel = MPIUtilities.getSlaves() > 0;
        } catch (MPIException mpiEx) {
            String msg = "Impossible to initialize MPI: " + mpiEx.getMessage();
            logger.info(msg);
            logger.info("Continuing as single machine");
        }

        if (myRank == MPIUtilities.MASTER) {
            EDGERemoteServerImpl factory = new EDGERemoteServerImpl();
            factory.start();

            if (argv.length == 1) {
                long timeout = 10000;
                try {
                    timeout = Long.parseLong(argv[0]);
                } catch (NumberFormatException e) {
                } finally {
                    Thread.sleep(timeout);
                }
                try {
                    factory.exit();
                } catch (RemoteException e) {
                    factory.forceExit();
                }
            }
        } else {
            EDGERemoteMPIImpl edgeRemote = new EDGERemoteMPIImpl();
            edgeRemote.computeLearning();
        }
    }

    public EDGERemoteServer start() {
        // location of this class
        String loc = EDGERemoteServerImpl.class.getProtectionDomain().getCodeSource().getLocation().getPath();

        // If java.security.policy option is not set, a default file will be created
        if (System.getProperty("java.security.policy") == null) {

            try {
                // this line must be used only for test
                //path = EDGERemoteServerImpl.createPolicyFile("file:" + System.getProperty("user.dir") + File.separator);
                //path = EDGERemoteServerImpl.createPolicyFile("file:");
                pathPolicyFile = createPolicyFile(policyCodebase);
            } catch (Exception ex) {
                logger.fatal(ex.getMessage(), ex.getCause());
                System.exit(-1);
            }
            System.setProperty("java.security.policy", pathPolicyFile);
            logger.debug("Setted default file policy " + pathPolicyFile);
        }

        // set useCodebaseOnly option to false
        if (System.getProperty("java.rmi.server.useCodebaseOnly") == null) {
            System.setProperty("java.rmi.server.useCodebaseOnly", "false");
        }

        if (System.getProperty("java.rmi.server.codebase") == null) {
            String codebase = EDGERemoteServer.class.getProtectionDomain().getCodeSource().getLocation().toString();
            codebase += " " + EDGEStatImpl.class.getProtectionDomain().getCodeSource().getLocation().toString();
            System.setProperty("java.rmi.server.codebase", codebase);
            logger.debug("Setted default codebase: " + codebase);
        }

        // set hostname
        if (System.getProperty("java.rmi.server.hostname") == null) {
            System.setProperty("java.rmi.server.hostname", "127.0.0.1");
        }

        // sets security manager
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        // 2 lines used to check permissions
        //String dir = System.getProperty("user.dir");
        //System.out.println("current dir = " + dir);
        EDGERemoteServer stub = null;
        try {
            //String name = "EDGERemoteServer";
            String name = EDGERemoteServer.class.getSimpleName();
            //EDGERemoteFactory factory = new EDGERemoteServerImpl();
            stub = (EDGERemoteServer) UnicastRemoteObject.exportObject((EDGERemoteServer) this, 0);

            try {
                logger.info("Launching RMIRegistry...");
                registry = LocateRegistry.createRegistry(port);
                //registry = LocateRegistry.getRegistry();
                logger.info("RMIRegistry launched!");
            } catch (RemoteException e) {
                logger.info("RMIRegistry at port " + port + " already launched. Getting reference...");
                registry = LocateRegistry.getRegistry();
                logger.info("RMIRegistry reference acquired");
            }
            registry.rebind(name, stub);
            logger.info("EDGE remote factory registered. Listening on port: " + port);
            registered = true;
        } catch (RemoteException e) {
            logger.fatal("Error registering EDGE remote factory: " + e.getMessage());
            //e.printStackTrace();
            System.exit(-1);
        } catch (Exception e) {
            logger.fatal(e.getMessage());
            System.exit(-1);
        }

        return stub;
    }

    /**
     * Create a temporary policy file.
     *
     * @param codeBase
     * @return the absolute pathPolicyFile of the temporary file
     * @throws java.lang.Exception
     */
    public static String createPolicyFile(String codeBase) throws Exception {
        //String str = " grant codeBase \"" + codeBase + "\" {\n"
        String str = " grant {\n"
                + "\tpermission java.security.AllPermission; \n"
                + "};";
        File policyFile = null;
        try {
            policyFile = File.createTempFile("edgeServer", ".policy");
            policyFile.deleteOnExit();
        } catch (IOException ex) {
            logger.fatal("Fatal error creating the temporary policy file", ex);
            throw new Exception("Fatal error creating the temporary policy file\n", ex);
        }

        try {
            PrintWriter writer = new PrintWriter(new FileWriter(policyFile));
            writer.println(str);
            writer.close();
        } catch (IOException ex) {
            System.out.println("Error writing policy file. See log for more details");
            logger.fatal(ex.getMessage());
            throw new Exception("Error writing policy file.", ex);
        }
        //}
        return policyFile.getAbsolutePath();
    }

    @Override
    public void exit() throws RemoteException {
        try {

            logger.info("Unbind RMIRegistry");
            // Unregister ourself
            registry.unbind(EDGERemoteServer.class.getSimpleName());
            // Unexport; this will also remove us from the RMI runtime
            //UnicastRemoteObject.unexportObject(this, true);
            UnicastRemoteObject.unexportObject((EDGERemoteServer) this, true);
            UnicastRemoteObject.unexportObject(registry, true);
            //Registry registry = LocateRegistry.getRegistry();

            logger.info(EDGERemoteServer.class.getSimpleName() + " exiting.");

        } catch (NotBoundException e) {
            logger.error(e.getMessage());
            throw new RemoteException("Could not unregister service, quitting anyway", e);
        } catch (RemoteException e) {
            logger.error(e.getMessage());
            throw new RemoteException("Could not unregister service, quitting anyway", e);
        } catch (Exception e) {
            logger.fatal(e.getMessage());
            throw new RemoteException(e.getMessage());
        }
        new Thread() {
            @Override
            public void run() {
                logger.info("Shutting down...");
                try {
                    sleep(2000);
                } catch (InterruptedException e) {
                    // I don't care
                }
                System.exit(0);
            }

        }.start();

    }

    @Override
    public void forceExit() throws RemoteException {
        System.exit(-1);
    }

    @Override
    public EDGERemote createEDGE() throws RemoteException {
        String client;
        try {
            client = RemoteServer.getClientHost();
        } catch (ServerNotActiveException e) {
            client = "remote";
        }
        logger.info("Request by: " + client);
        EDGERemote stub = null;
        EDGERemoteImpl instance = new EDGERemoteImpl();
        instance.setPolicyFile(pathPolicyFile);
        try {
            stub = (EDGERemote) UnicastRemoteObject.exportObject(instance, 0);
        } catch (RemoteException e) {
            //System.out.println("Fatal Error: error retriving EDGE remote reference");
            logger.fatal("Error retriving EDGE remote reference: " + e.getMessage());
        }
        return stub;
    }

    public EDGERemote createEDGE(EDGERemote.EDGEMPISchedulingType schedType) {

    }

    public void setPolicyCodebase(String policyCodebase) {
        this.policyCodebase = policyCodebase;
    }

    @Override
    public InputStream getInputStream(File f) throws IOException, RemoteException {
        return Communication.getInputStream(f);
    }

    @Override
    public OutputStream getOutputStream(File f) throws IOException, RemoteException {
        return Communication.getOutputStream(f);
    }

//    @Override
//    public InputStream getInputStream() throws IOException, RemoteException {
//        return Communication.getInputStream();
//    }
//
//    @Override
//    public OutputStream getOutputStream() throws IOException, RemoteException {
//        return Communication.getOutputStream();
//    }
}

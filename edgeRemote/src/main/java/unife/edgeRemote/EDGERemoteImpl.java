/**
 * This file is part of LEAP.
 *
 * LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, but
 * some components can be used as stand-alone.
 *
 * LEAP is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * LEAP is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package unife.edgeRemote;

import unife.edgeRemote.exceptions.InconsistencyException;
//import org.dllearner.utilities.owl.OWLAPIDescriptionConvertVisitor;
import unife.utilities.OWLAPIDescriptionConvertVisitor2;
import unife.utilities.Utility;
import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import java.rmi.RemoteException;
import java.util.Collections;
import java.util.Stack;
import org.apache.log4j.Logger;
import org.dllearner.core.owl.Description;
import org.dllearner.core.owl.NamedClass;
import org.semanticweb.owlapi.apibinding.OWLManager;

import org.semanticweb.owlapi.io.OWLFunctionalSyntaxOntologyFormat;
import org.semanticweb.owlapi.io.OWLXMLOntologyFormat;
import org.semanticweb.owlapi.io.RDFXMLOntologyFormat;
import org.semanticweb.owlapi.io.StringDocumentSource;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.OWLAnnotationProperty;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassAssertionAxiom;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLException;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLOntologyChangeException;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyFormat;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;
import unife.bundle.logging.BundleLoggerFactory;
import unife.edge.EDGE;
import unife.edge.EDGEStat;
import unife.rmi.communication.RMIInputStreamImpl;
import unife.rmi.communication.RMISerializableInputStream;

/**
 * This class is the implementation of EDGERemote Interface. It implements the
 * EDGE algorithm
 *
 * @author Giuseppe Cota <giuseta@gmail.com>, Riccardo Zese
 * <riccardo.zese@unife.it>
 */
public class EDGERemoteImpl implements EDGERemote {

    //private static final long serialVersionUID = 1357908642L;
    private static Logger logger = Logger.getLogger(EDGERemoteImpl.class.getName(), new BundleLoggerFactory());

    private String policyFile;
    // object of the class that performs the EDGE algorithm
    private final EDGE edge;
    // background knowledge
    //private OWLOntology baseOntology;
    // probabilistic added axioms
    private Stack<OWLAxiom> probAddedAxioms = new Stack<OWLAxiom>();
    // ontology manager
    private final OWLOntologyManager OWLmanager;
    // dummy class used during the learning
    private OWLClass dummyClass;
    // Last axiom added into the probabilistic current ontology
    private OWLAxiom probAddedAxiom = null;
    // output format of the learned ontology
    OWLOntologyFormat outFormat = new OWLXMLOntologyFormat();

    boolean updated;
    // class we want to describe
    OWLClass classToDescribe;
    //private Boolean keepParameters;

    // ontology containing the positive examples
    private OWLOntology positiveOntology = null;
    // ontology containing the negative examples
    private OWLOntology negativeOntology = null;

    /**
     * prefix used for the dummy class.
     *
     */
    protected static final String PREFIX = "https://sites.google.com/a/unife.it/ml/bundle";

    /**
     * @param policyFile the policyFile to set
     */
    public void setPolicyFile(String policyFile) {
        this.policyFile = policyFile;
    }

    /**
     *
     * @throws RemoteException
     */
    public EDGERemoteImpl() throws RemoteException {
        edge = new EDGE();

        if (policyFile != null) {
            System.setProperty("java.security.policy", policyFile);
        }
        // sets security manager
//        if (System.getSecurityManager() == null) {
//            System.setSecurityManager(new SecurityManager());
//        }
        OWLmanager = OWLManager.createOWLOntologyManager();
//        try {
//            updated = true;
//        } catch (OWLOntologyCreationException ex) {
//            logger.fatal("Error creating the probabilistic ontology", ex);
//            throw new RemoteException("Error creating the probabilistic ontology");
//        }
        // create dummy Class
        dummyClass = createDummyClass();
    }

    @Override
    public void setConceptToDescribe(Description concept) throws RemoteException {
        try {
            logger.debug("Converting atomic class from DL-Learner description to OWLAPI decription based");
            classToDescribe = (OWLClass) OWLAPIDescriptionConvertVisitor2.getOWLClassExpression(concept);
            logger.debug("Conversion successful");
        } catch (Exception e) {
            logger.fatal("Exception during DL-Learner expressions conversion", e);
            throw new RemoteException("Exception during DL-Learner expressions conversion", e);
        }
    }

    @Override
    public OWLOntology getLearnedOntology() throws RemoteException {
        logger.info("Getting the complete learned ontology");
        OWLOntology ontology;
        if (updated) {
            ontology = edge.getOntology();
        } else {
            ontology = edge.getLearnedOntology();
        }
        int numUpdatedAxioms = 0;
        if (classToDescribe != null) {
            // It needs to replace the super class (the dummy class) of the learned subclassof axiom 
            // with the class we want to describe
            logger.info("Updating the axioms' superclasses...");
            OWLDataFactory factory = OWLmanager.getOWLDataFactory();
//            if (!updated) {
//                updateOntology();
//            }
            List<OWLAxiom> probAddedAxiomsCopy = new ArrayList<OWLAxiom>(probAddedAxioms);
            for (OWLAxiom axiom : ontology.getAxioms()) {
                logger.debug("Axiom of ontologyProb: " + Utility.getManchesterSyntaxString(axiom));
                if (probAddedAxiomsCopy.size() < 1) {
                    break;
                }

                for (OWLAxiom addAx : probAddedAxiomsCopy) { // conviene usare una copia di probAddedAxioms 
                    //in maniera tale da eliminare gli assiomi gi� trovati durante la ricerca e 
                    //quindi ridurre il numero di check
                    logger.debug("Added axiom: " + Utility.getManchesterSyntaxString(addAx));
                    if (addAx.equalsIgnoreAnnotations(axiom)) {
                        logger.debug("EQUALS!");
                        OWLClassExpression classExpression = ((OWLSubClassOfAxiom) axiom).getSubClass();
                        OWLAnnotationProperty annotationProperty = factory.getOWLAnnotationProperty(IRI.create(PREFIX + "#probability"));
                        Set<OWLAnnotation> annotation = axiom.getAnnotations(annotationProperty);
                        OWLAxiom tempAxiom = factory.getOWLSubClassOfAxiom(classExpression, classToDescribe);
                        OWLmanager.removeAxiom(ontology, axiom);
                        OWLmanager.addAxiom(ontology, tempAxiom.getAnnotatedAxiom(annotation));
                        probAddedAxiomsCopy.remove(addAx);
                        numUpdatedAxioms++;
                        break;
                    }
                }
            }
            logger.debug("Number of axioms' superclasses updated: " + numUpdatedAxioms);
        }
        updated = true;
        return ontology;
    }

    @Override
    public InputStream getLearnedOntologyInputStream() throws RemoteException, IOException {
        final OWLOntology learnedOntology;
        if (updated) {
            learnedOntology = edge.getOntology();
        } else {
            learnedOntology = edge.getLearnedOntology();
        }
        if (!learnedOntology.equals(edge.getLearnedOntology())) {
            String msg = "Different ontology objects!";
            logger.error(msg);
            throw new RemoteException(msg);
        }
        final PipedOutputStream outputStream = new PipedOutputStream();
        PipedInputStream pipedInputStream = new PipedInputStream(outputStream);
        //OntologyThread ontologyThread = new OntologyThread(manager, completeLearnedOntology, outputStream);
        //ontologyThread.start();
        new Thread() {
            @Override
            public void run() {
                try {
                    OWLmanager.saveOntology(learnedOntology, outFormat, outputStream);
                    outputStream.close();
                } catch (OWLOntologyStorageException e) {
                    String message = "Error during the streaming of the complete learned ontology";
                    logger.error(message, e);
                    //throw new RemoteException(message, e);
                    System.exit(-1);
                } catch (IOException e) {
                    logger.error(e.getMessage());
                    System.exit(-1);
                }
            }
        }.start();
        return new RMISerializableInputStream(new RMIInputStreamImpl(pipedInputStream));
    }

//    @Override
//    public void addAxiom(String ax) throws RemoteException, InconsistencyException {
//        updated = false;
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
    @Override
    public void addSubClassOfAxiom(Description description, double epistemicProb) throws RemoteException, InconsistencyException {

        OWLClassExpression classExpressionFound = null; // class expression found by the upper level algorithm
        try {
            logger.debug("Converting Class expression from DL-Learner description to OWLAPI decription based");
            classExpressionFound = OWLAPIDescriptionConvertVisitor2.getOWLClassExpression(description);
            logger.debug("Conversion successful");
        } catch (Exception e) {
            logger.fatal("Exception during DL-Learner expressions conversion", e);
            throw new RemoteException("Exception during DL-Learner expressions conversion", e);
        }

        // adding axiom classExpressionFound subClassOf dummyClass
        addSubClassOfAxiom(classExpressionFound, dummyClass, epistemicProb);
    }

    @Override
    public void addSubClassOfAxiom(Description subClass, Description superClass, double epistemicProb) throws RemoteException, InconsistencyException {

        OWLClassExpression target = null;
        OWLClassExpression classExpressionFound = null; // class expression found by the upper level algorithm

        try {
            logger.debug("Converting Class expressions from DL-Learner description to OWLAPI decription based");
            classExpressionFound = OWLAPIDescriptionConvertVisitor2.getOWLClassExpression(subClass);
            target = OWLAPIDescriptionConvertVisitor2.getOWLClassExpression(superClass);
            logger.debug("Conversion successful");
        } catch (Exception e) {
            logger.fatal("Exception during DL-Learner expressions conversion", e);
            throw new RemoteException("Exception during DL-Learner expressions conversion", e);
        }
        // adding subClassOf axiom
        addSubClassOfAxiom(classExpressionFound, target, epistemicProb);
    }

    /**
     * Private method used to add a subClassOf axiom.
     *
     * @param subClass
     * @param superClass
     * @param epistemicProb
     * @throws RemoteException
     */
    private void addSubClassOfAxiom(OWLClassExpression subClass, OWLClassExpression superClass, double epistemicProb) throws RemoteException, InconsistencyException {
        OWLOntology baseOntology = edge.getOntology();
        if (baseOntology == null) {
            logger.error("ERROR! Base Ontology not set");
            throw new RemoteException("ERROR! Base Ontology not set");
        }

        // Consistency check before adding
        if (classToDescribe != null) {
            OWLAxiom axiomConsTest = OWLmanager.getOWLDataFactory().getOWLSubClassOfAxiom(subClass, classToDescribe);
            logger.info("Consistency check of the axiom: " + Utility.getManchesterSyntaxString(axiomConsTest));
            OWLmanager.addAxiom(baseOntology, axiomConsTest);
            //OWLReasonerFactory reasonerFactory = new StructuralReasonerFactory();
            //OWLReasonerConfiguration configReasoner = new SimpleConfiguration();
            //OWLReasoner reasoner = reasonerFactory.createReasoner(ontologyOWL, configReasoner);
            //reasoner.precomputeInferences();
            PelletReasoner pelletReasoner = new PelletReasonerFactory().createReasoner(baseOntology);
            //logger.info("consistency: "+ reasoner.isConsistent());
            if (!pelletReasoner.isConsistent()) {
                String message = "The axiom will make the KB inconsistent.\n"
                        + "It will NOT be added";
                logger.info(message);
                OWLmanager.removeAxiom(baseOntology, axiomConsTest);
                throw new InconsistencyException(message);
            } else {
                OWLmanager.removeAxiom(baseOntology, axiomConsTest);
            }
        }
        int numAxiomOld = baseOntology.getAxiomCount();

        logger.info("Adding axiom into the current ontology");

        OWLDataFactory factory = OWLmanager.getOWLDataFactory();
        OWLAxiom axiomToAddAnn = null;
        try {
            // adding axiom classExpressionFound subClassOf dummyClass
            OWLAxiom axiomToAdd = factory.getOWLSubClassOfAxiom(subClass, superClass);
            logger.info("Adding axiom into complete ontology: " + Utility.getManchesterSyntaxString(axiomToAdd));
            OWLAnnotationProperty prop = factory.getOWLAnnotationProperty(IRI.create(PREFIX + "#probability"));
            OWLAnnotation annotation = factory.getOWLAnnotation(prop, factory.getOWLLiteral(epistemicProb));
            // create annotated axiom
            axiomToAddAnn = axiomToAdd.getAnnotatedAxiom(Collections.singleton(annotation));

//            if (keepParameters) {
//                OWLOntology probOntology = edge.getProbOntology();
//                if (probOntology == null) {
//                    logger.error("ERROR! It was told to keep the old parameters "
//                            + "but the probabilistic ontology was not set");
//                    throw new RemoteException("ERROR! Probabilistic Ontology not set");
//                }
//                OWLmanager.addAxiom(probOntology, axiomToAdd);
//            }
            OWLmanager.addAxiom(baseOntology, axiomToAddAnn);
            updated = false;
        } catch (OWLOntologyChangeException e) {
            logger.fatal("Error adding the axiom into the current ontology", e);
            throw new RemoteException("Error adding the axiom into the current ontology", e);
        } catch (Exception e) {
            logger.fatal("Unknown exception", e);
            throw new RemoteException("Unknown exception", e);
        }

        probAddedAxiom = axiomToAddAnn;
        probAddedAxioms.add(axiomToAddAnn);
        logger.info("Axiom added into the current ontology");
        logger.info("Number of axioms added: " + (baseOntology.getAxiomCount() - numAxiomOld));
    }

    @Override
    public void removeLastAxiom() throws RemoteException {
        OWLOntology ontology = edge.getOntology();
        logger.debug("Removing last added axiom");
        String probAddedAxiomString = "";
        int numAxiomOld = ontology.getAxiomCount();
        try {
            if (probAddedAxiom == null) {
                logger.error("Trying to remove an axiom not added");
                throw new RemoteException("Trying to remove an axiom not added");
            }
            probAddedAxiomString = Utility.getManchesterSyntaxString(
                    probAddedAxiom.getAxiomWithoutAnnotations());

            logger.info("Removing axioms: " + probAddedAxiomString);
            OWLmanager.removeAxiom(ontology, probAddedAxiom);
//            if (keepParameters) {
//                OWLOntology probOntology = edge.getProbOntology();
//                if (probOntology == null) {
//                    logger.error("ERROR! It was told to keep the old parameters "
//                            + "but the probabilistic ontology was not set");
//                    throw new RemoteException("ERROR! Probabilistic Ontology not set");
//                }
//                OWLmanager.removeAxiom(probOntology, probAddedAxiom);
//            }
            probAddedAxioms.pop();
            //updated = true;
            // extract index
            int i = 0;
            OWLAnnotationProperty prop = OWLManager.getOWLDataFactory()
                    .getOWLAnnotationProperty(IRI.create(PREFIX + "#probability"));
            // DA TESTARE!!!!
            for (OWLAxiom ax : ontology.getAxioms(prop)) {
                if (probAddedAxiom.equalsIgnoreAnnotations(ax)) {
                    break;
                }
            }

            probAddedAxiom = null;
            logger.info("Last axiom removed!");
            logger.info("Axiom removed: " + (numAxiomOld - edge.getOntology().getAxiomCount()));
            updated = true;
            //updateOntology();
        } catch (OWLOntologyChangeException e) {
            String message = "Error removing the axiom \"" + probAddedAxiomString
                    + "\" from the current ontology";
            logger.error(message + "\n" + e.getMessage());
            throw new RemoteException(message, e);
        }
    }

    @Override
    public void setOntology(String ontologyString) throws RemoteException {
        logger.info("Creating ontology from string...");
        try {
            edge.setOntology(OWLmanager.loadOntologyFromOntologyDocument(new StringDocumentSource(ontologyString)));
        } catch (OWLOntologyCreationException e) {
            logger.error("Loading of the string ontology failed: " + e.getMessage());
            throw new RemoteException("Loading of the string ontology failed", e);
        }
    }

    @Override
    public void setOntology(OWLOntology ontology) throws RemoteException {
        logger.info("Loading ontology...");
        edge.setOntology(ontology);
        logger.info("Ontology loaded!");
    }

    @Override
    public void setOntology(File fi) throws IOException, RemoteException {
        logger.info("Loading ontology...");
        String path = fi.getCanonicalPath();
        String filename = path.substring(path.lastIndexOf("/"));
        String directory = "tmp/";
        File f = new File(directory + filename);
        if (f.exists()) {
            try {
                edge.setOntology(OWLmanager.loadOntologyFromOntologyDocument(f));
            } catch (OWLOntologyCreationException ex) {
                logger.error("Error during the loading of the base ontology");
                throw new RemoteException("Error during the loading of the base ontology\n"
                        + ex.getMessage());
            }
        } else {
            logger.error("The given file does not exist. It is impossible to load the base ontology");
            throw new IOException("Given file does not esist. Upload first!");
        }
        logger.info("Ontology loaded successfully");
    }

    /**
     * Create a list Class Assertion axioms.
     *
     * @param individuals
     * @param classAsserted
     * @return
     */
    private Set<OWLAxiom> createExamples(Set<String> individuals, OWLClassExpression classAsserted) {
        //OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        OWLDataFactory factory = OWLmanager.getOWLDataFactory();
        Set<OWLAxiom> examples = new HashSet<OWLAxiom>();
        for (String ind : individuals) {
            OWLNamedIndividual individual = factory.getOWLNamedIndividual(IRI.create(ind));
            OWLClassAssertionAxiom ax = factory.getOWLClassAssertionAxiom(classAsserted, individual);
            examples.add(ax);
        }
        return examples;
    }

    @Override
    public void setPositiveExamples(Set<String> positiveIndividuals) throws RemoteException {
        logger.info("Setting positive examples...");
        /* 
         if (ontologyOWL == null) {
         logger.error("ERROR! Input Ontology not set");
         throw new RemoteException("ERROR! Input Ontology not set");
         }
         */
        dummyClass = createDummyClass();
        List<OWLAxiom> pExamples
                = new ArrayList<OWLAxiom>(createExamples(positiveIndividuals, dummyClass));
        edge.setPositiveExamples(pExamples);
        logger.info("Positive Examples set");
    }

    /**
     *
     * @param positiveIndividuals
     * @param namedClass
     * @throws RemoteException
     */
    @Override
    public void setPositiveExamples(Set<String> positiveIndividuals, NamedClass namedClass) throws RemoteException {
        /* to uncomment in future
         OWLClassExpression classAsserted=null;
         try {
         logger.debug("Converting Class from DL-Learner description to OWLAPI decription based");
         classAsserted = OWLAPIDescriptionConvertVisitor2.getOWLClassExpression(namedClass);
         logger.debug("Conversion successful");
         } catch (Exception e) {
         logger.fatal("Exception during DL-Learner expressions conversion", e);
         throw new RemoteException("Exception during DL-Learner expressions conversion", e);
         }
         positiveExamples = createExamples(positiveIndividuals, classAsserted);
         */
        // to comment in future
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setNegativeExamples(Set<String> negativeIndividuals) throws RemoteException {
        logger.info("Setting negative examples...");
        /*
         if (ontologyOWL == null) {
         logger.error("ERROR! Input Ontology not set");
         throw new RemoteException("ERROR! Input Ontology not set");
         }
         */
        dummyClass = createDummyClass();
        List<OWLAxiom> nExamples = new ArrayList<OWLAxiom>(createExamples(negativeIndividuals, dummyClass));
        edge.setNegativeExamples(nExamples);
        logger.info("Negative examples set");
    }

    @Override
    public void setNegativeExamples(Set<String> negativeIndividuals, Description superClass) throws RemoteException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setSeed(int seed) throws RemoteException {
        edge.setSeed(seed);
    }

    @Override
    public void setRandomize(boolean randomize) throws RemoteException {
        edge.setRandomize(randomize);
    }

    @Override
    public void setRandomizeAll(boolean randomizeAll) throws RemoteException {
        edge.setRandomizeAll(randomizeAll);
    }

    @Override
    public void setDifferenceLL(String diff) throws RemoteException {
        try {
            edge.setDiffLL(diff);
        } catch (Exception e) {
            String message = "Wrong Log-Likelihood difference value: " + diff + ".\nIt must be a real "
                    + "number greater that 0. Default value will be used";
            logger.error(message);
        }
    }

    @Override
    public void setRatioLL(String ratio) throws RemoteException {
        try {
            edge.setRatioLL(ratio);
        } catch (Exception e) {
            String message = "Wrong ratio value: " + ratio + ".\nIt must be a real "
                    + "number greater that 0 and less or equal than 1. Default value will be used";
            logger.error(message);
        }
    }

    @Override
    public void setMaxIterations(long iter) throws RemoteException {
        if (iter <= 0) {
            String message = "Wrong max number of iterations: " + iter + ".\nIt must be a natural "
                    + "number greater that 0. Default value: " + edge.getMaxIterations() + " will be used";
            logger.error(message);
            return;
        }
        edge.setMaxIterations(iter);
    }

    @Override
    public void setMaxExplanations(int maxEx) throws RemoteException {
        if (maxEx <= 0) {
            String message = "Wrong man number of explanations: " + maxEx + ".\nIt must be a natural "
                    + "number greater that 0. Default value: " + edge.getMaxExplanations() + " will be used";
            logger.error(message);
            return;
        }
        edge.setMaxExplanations(maxEx);
    }

    @Override
    public void setTimeout(int timeout) throws RemoteException {
        if (timeout == 0) {
            return;
        }
        if (timeout < 0) {
            String message = "Wrong man timeout (in seconds) value: " + timeout + ".\nIt must be a natural "
                    + "number equal or greater that 0 (0 = infinite timeout). No timeout set";
            logger.error(message);
            return;
        }
        edge.setTimeOut("" + timeout);
    }

    @Override
    public void setShowAll(boolean showAll) throws RemoteException {
        edge.setShowAll(showAll);
    }

    @Override
    public void setOutputFileFormat(PossibleOutputFileFormat format) throws RemoteException {
        switch (format) {
            case OWLXML:
                outFormat = new OWLXMLOntologyFormat();
                break;
            case OWLFUNCTIONAL:
                outFormat = new OWLFunctionalSyntaxOntologyFormat();
                break;
            case RDFXML:
                outFormat = new RDFXMLOntologyFormat();
                break;
            default:
                break;
        }
    }

    @Override
    public void setMerge(boolean merge) {
        edge.setMerge(merge);
    }

    @Override
    public void updateOntology() throws RemoteException {
        // List of updated axioms.
        // Updating the probability values in the learned Ontology
        logger.info("");
        logger.info("Updating probability values of the axioms...");
        OWLOntology updatedOntology = edge.getLearnedOntology();
        edge.setOntology(updatedOntology);
//        if (keepParameters) {
//            OWLOntology 
//        }
        updated = true;
        logger.info("Number of axioms updated: " + Utility.getProbabilisticAxioms(updatedOntology).size());
    }

    public EDGEStat run() throws RemoteException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private void init() throws RemoteException, OWLOntologyCreationException {
//        if (keepParameters) {
//            OWLOntology probOntology = OWLmanager.createOntology(IRI.create(PREFIX));
//            edge.setProbOntology(probOntology);
//        }
    }

    private synchronized OWLClass createDummyClass() {
        if (dummyClass == null) {
            // create dummy class
//            if (classToDescribe != null) {
//                logger.debug("Creating dummy class for " + ((OWLClass) classToDescribe).toStringID());
//            } else {
            logger.debug("Creating dummy class");

//            }
            // It must be very rare that a class in an ontology has the IRI defined by PREFIX#learnedClass
            // but let's make it sure
            int i = 0;
            String dummyClassStringIRI = PREFIX + "#learnedClass";
//            String num = "";
//            while (ontologyOWL.containsClassInSignature(IRI.create(dummyClassStringIRI + num))) {
//                num = "" + i;
//                i++;
//            }
//            dummyClass = manager.getOWLDataFactory().getOWLClass(IRI.create(dummyClassStringIRI + num));
            dummyClass = OWLmanager.getOWLDataFactory().getOWLClass(IRI.create(dummyClassStringIRI));
            logger.debug("Dummy class created");
            return dummyClass;
        } else {
            return dummyClass;
        }
    }

    @Override
    public String getName() {
        return this.getClass().getSimpleName();
    }

//    @Override
//    public void setKeepParameters(Boolean keepParameters) throws RemoteException {
//        this.keepParameters = keepParameters;
//    }
    @Override
    public EDGEStat computeLearning() throws RemoteException {
        try {
            return edge.computeLearning();
        } catch (Exception e) {
            logger.error(e.getMessage());
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            logger.error(sw);
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public List<OWLAxiom> getNegativeExamples() throws RemoteException {
        return edge.getNegativeExamples();
    }

    @Override
    public OWLOntology getNegativeExamplesOntology() throws RemoteException {
        //OWLOntology negativeOntology = null;
        try {
            if (negativeOntology == null) {
                negativeOntology = getExamplesOntology(edge.getNegativeExamples());
            }
        } catch (OWLOntologyCreationException e) {
            String message = "Error during the creation of the ontology containing the positive examples";
            logger.info(message, e);
            throw new RemoteException(message, e);
        } catch (OWLException e) {
            logger.info(e.getMessage());
            throw new RemoteException(e.getMessage());
        }
        return negativeOntology;
    }

    @Override
    public InputStream getNegativeExamplesInputStream() throws RemoteException, IOException {
        logger.debug("Getting Input stream for reading negative examples...");
        //final OWLOntology negativeOntology;
        try {
            if (negativeOntology == null) {
                negativeOntology = getExamplesOntology(edge.getNegativeExamples());
            }
        } catch (OWLException e) {
            throw new RemoteException(e.getMessage());
        }
        final PipedOutputStream outputStream = new PipedOutputStream();
        PipedInputStream pipedInputStream = new PipedInputStream(outputStream);
        new Thread() {
            @Override
            public void run() {
                try {
                    OWLmanager.saveOntology(negativeOntology, outFormat, outputStream);
                    outputStream.close();
                } catch (OWLOntologyStorageException e) {
                    String message = "Error during the streaming of the ontology containing the negative examples";
                    logger.error(message, e);
                    //throw new RemoteException(message, e);
                    System.exit(-1);
                } catch (IOException e) {
                    logger.error(e.getMessage());
                    System.exit(-1);
                }
            }
        }.start();
        return new RMISerializableInputStream(new RMIInputStreamImpl(pipedInputStream));
    }

    @Override
    public List<OWLAxiom> getPositiveExamples() throws RemoteException {
        return edge.getPositiveExamples();
    }

    @Override
    public OWLOntology getPositiveExamplesOntology() throws RemoteException {
        //OWLOntology positiveOntology = null;
        try {
            if (positiveOntology == null) {
                positiveOntology = getExamplesOntology(edge.getPositiveExamples());
            }
        } catch (OWLOntologyCreationException e) {
            String message = "Error during the creation of the ontology containing the positive examples";
            logger.error(message, e);
            throw new RemoteException(message, e);
        } catch (OWLException e) {
            logger.error(e.getMessage());
            throw new RemoteException(e.getMessage());
        }
        return positiveOntology;
    }

    @Override
    public InputStream getPositiveExamplesInputStream() throws RemoteException, IOException {
        logger.debug("Getting Input stream for reading positive examples...");
        //final OWLOntology positiveOntology;
        try {
            if (positiveOntology == null) {
                positiveOntology = getExamplesOntology(edge.getPositiveExamples());
            }
        } catch (OWLException e) {
            throw new RemoteException(e.getMessage());
        }
        final PipedOutputStream outputStream = new PipedOutputStream();
        PipedInputStream pipedInputStream = new PipedInputStream(outputStream);
        new Thread() {
            @Override
            public void run() {
                try {
                    OWLmanager.saveOntology(positiveOntology, outFormat, outputStream);
                    outputStream.close();
                } catch (OWLOntologyStorageException e) {
                    String message = "Error during the streaming of the ontology containing the positive examples";
                    logger.error(message, e);
                    //throw new RemoteException(message, e);
                    System.exit(-1);
                } catch (IOException e) {
                    logger.error(e.getMessage());
                    System.exit(-1);
                }
            }
        }.start();

        return new RMISerializableInputStream(new RMIInputStreamImpl(pipedInputStream));
    }

    private OWLOntology getExamplesOntology(List<OWLAxiom> examples) throws OWLOntologyCreationException, OWLException {
        OWLOntology examplesOntology = OWLmanager.createOntology();
        List<OWLAxiom> updatedExamples = new ArrayList<OWLAxiom>();
        if (classToDescribe != null) {
            OWLDataFactory factory = OWLmanager.getOWLDataFactory();
            for (OWLAxiom axiom : examples) {
                if (!axiom.isOfType(AxiomType.CLASS_ASSERTION)) {
                    throw new OWLOntologyCreationException("The example is not a class assertion axiom");
                }
                OWLIndividual individual = ((OWLClassAssertionAxiom) axiom).getIndividual();
                OWLAxiom updatedExample = factory.getOWLClassAssertionAxiom(classToDescribe, individual);
                updatedExamples.add(updatedExample);
            }
        } else {
            updatedExamples = examples;
        }
        OWLmanager.addAxioms(examplesOntology, new HashSet<OWLAxiom>(updatedExamples));
        return examplesOntology;
    }

}

/**
 *  This file is part of LEAP.
 * 
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, 
 *  but some components can be used as stand-alone.
 * 
 *  LEAP is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  LEAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package unife.bundle.monitor;

import com.clarkparsia.owlapi.explanation.io.ExplanationRenderer;
import com.clarkparsia.owlapi.explanation.io.manchester.ManchesterSyntaxExplanationRenderer;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLException;

/**
 *
 * @author Riccardo Zese <riccardo.zese@unife.it>, Giuseppe Cota <giuseta@gmail.com>
 */
public class ScreenRendererTimeExplanationProgressMonitor
        extends TimeMonitorImpl
        implements BundleRendererExplanationProgressMonitor {

    private ExplanationRenderer rend = new ManchesterSyntaxExplanationRenderer();
    private OWLAxiom axiom;
    private Set<Set<OWLAxiom>> setExplanations;
    private PrintWriter pw;

    public ScreenRendererTimeExplanationProgressMonitor(OWLAxiom axiom) {
        this.axiom = axiom;
        this.pw = new PrintWriter(System.out);

        setExplanations = new HashSet<>();
        try {
            rend.startRendering(pw);

        } catch (OWLException | IOException e) {
            throw new RuntimeException("Error rendering explanation: " + e);
        }
    }

//    public String RendererExplanationProgressMonitorString(OWLAxiom axiom) {
//        this.axiom = axiom;
//        StringWriter stringWriter = new StringWriter();
//        this.pw = new PrintWriter(stringWriter);
//
//        setExplanations = new HashSet<>();
//        try {
//            rend.startRendering(pw);
//            return stringWriter.toString();
//
//        } catch (OWLException | IOException e) {
//            throw new RuntimeException("Error rendering explanation: " + e);
//        }
//    }
    @Override
    public void foundExplanation(Set<OWLAxiom> axioms) {

        if (!setExplanations.contains(axioms)) {
            setExplanations.add(axioms);
            pw.flush();
            try {
                rend.render(axiom, Collections.singleton(axioms));
            } catch (IOException | OWLException e) {
                throw new RuntimeException("Error rendering explanation: " + e);
            }
        }
    }

    @Override
    public void foundAllExplanations() {
        try {
            rend.endRendering();
        } catch (OWLException | IOException e) {
            System.err.println("Error rendering explanation: " + e);
        }
    }

    @Override
    public void foundNoExplanations() {
        try {
            rend.render(axiom, Collections.<Set<OWLAxiom>>emptySet());
            rend.endRendering();
            rend = null;
        } catch (OWLException e) {
            System.err.println("Error rendering explanation: " + e);
        } catch (IOException e) {
            System.err.println("Error rendering explanation: " + e);
        }
    }

    @Override
    public void write(String s) {
        pw.print(s);
    }

    @Override
    public void writeln(String s) {
        pw.println(s);
    }

}

/**
 *  This file is part of LEAP.
 * 
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, 
 *  but some components can be used as stand-alone.
 * 
 *  LEAP is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  LEAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package unife.rmi.communication;

import java.io.IOException;
import java.io.InputStream;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 *
 * @author Giuseppe Cota <giuseta@gmail.com>, Riccardo Zese <riccardo.zese@unife.it>
 */
public class RMIInputStreamImpl implements RMIInputStream {

    private final InputStream in;
    private byte[] b;

    public RMIInputStreamImpl(InputStream in) throws IOException {
        this.in = in;
        
        UnicastRemoteObject.exportObject(this, 1099);// è necessario????
    }

    @Override
    public void close() throws IOException, RemoteException {

        in.close();

    }

    @Override
    public int read() throws IOException, RemoteException {
        return in.read();

    }

    @Override
    public byte[] readBytes(int len) throws IOException, RemoteException {
        if (b == null || b.length != len) {
            b = new byte[len];
        }
        int len2 = 0;

        len2 = in.read(b);

        if (len2 < 0) {
            return null; // EOF reached
        }
        if (len2 != len) {
            // copy bytes to byte[] of correct length and return it
            byte[] b2 = new byte[len2];
            System.arraycopy(b, 0, b2, 0, len2);
            return b2;
        } else {
            return b;
        }
    }
    
    public RMIPipe transfer(int key) throws IOException, RemoteException{
        return new RMIPipe(key, in);
    }
}

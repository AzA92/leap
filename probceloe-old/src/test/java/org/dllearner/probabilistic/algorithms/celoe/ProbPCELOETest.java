/**
 *  This file is part of LEAP.
 * 
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, 
 *  but some components can be used as stand-alone.
 * 
 *  LEAP is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  LEAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.dllearner.probabilistic.algorithms.celoe;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import junit.framework.TestCase;
import org.dllearner.algorithms.celoe.OEHeuristicRuntime;
import org.dllearner.algorithms.celoe.OENode;
import org.dllearner.core.EvaluatedDescription;
import org.dllearner.core.owl.Description;
import org.dllearner.core.owl.NamedClass;
import org.dllearner.refinementoperators.LengthLimitedRefinementOperator;
import org.dllearner.refinementoperators.RefinementOperator;

/**
 *
 * @author Giuseppe Cota <giuseta@gmail.com>
 */
public class ProbPCELOETest extends TestCase {
    
    public ProbPCELOETest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of getName method, of class ProbPCELOE.
     */
    public void testGetName() {
        System.out.println("getName");
        String expResult = "";
        String result = ProbPCELOE.getName();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of init method, of class ProbPCELOE.
     */
    public void testInit() throws Exception {
        System.out.println("init");
        ProbPCELOE instance = new ProbPCELOE();
        instance.init();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getCurrentlyBestDescription method, of class ProbPCELOE.
     */
    public void testGetCurrentlyBestDescription() {
        System.out.println("getCurrentlyBestDescription");
        ProbPCELOE instance = new ProbPCELOE();
        Description expResult = null;
        Description result = instance.getCurrentlyBestDescription();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getCurrentlyBestDescriptions method, of class ProbPCELOE.
     */
    public void testGetCurrentlyBestDescriptions() {
        System.out.println("getCurrentlyBestDescriptions");
        ProbPCELOE instance = new ProbPCELOE();
        List<Description> expResult = null;
        List<Description> result = instance.getCurrentlyBestDescriptions();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getCurrentlyBestEvaluatedDescription method, of class ProbPCELOE.
     */
    public void testGetCurrentlyBestEvaluatedDescription() {
        System.out.println("getCurrentlyBestEvaluatedDescription");
        ProbPCELOE instance = new ProbPCELOE();
        EvaluatedDescription expResult = null;
        EvaluatedDescription result = instance.getCurrentlyBestEvaluatedDescription();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getCurrentlyBestEvaluatedDescriptions method, of class ProbPCELOE.
     */
    public void testGetCurrentlyBestEvaluatedDescriptions() {
        System.out.println("getCurrentlyBestEvaluatedDescriptions");
        ProbPCELOE instance = new ProbPCELOE();
        TreeSet<? extends EvaluatedDescription> expResult = null;
        TreeSet<? extends EvaluatedDescription> result = instance.getCurrentlyBestEvaluatedDescriptions();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getCurrentlyBestAccuracy method, of class ProbPCELOE.
     */
    public void testGetCurrentlyBestAccuracy() {
        System.out.println("getCurrentlyBestAccuracy");
        ProbPCELOE instance = new ProbPCELOE();
        double expResult = 0.0;
        double result = instance.getCurrentlyBestAccuracy();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of start method, of class ProbPCELOE.
     */
    public void testStart() {
        System.out.println("start");
        ProbPCELOE instance = new ProbPCELOE();
        instance.start();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of isRunning method, of class ProbPCELOE.
     */
    public void testIsRunning() {
        System.out.println("isRunning");
        ProbPCELOE instance = new ProbPCELOE();
        boolean expResult = false;
        boolean result = instance.isRunning();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of stop method, of class ProbPCELOE.
     */
    public void testStop() {
        System.out.println("stop");
        ProbPCELOE instance = new ProbPCELOE();
        instance.stop();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSearchTreeRoot method, of class ProbPCELOE.
     */
    public void testGetSearchTreeRoot() {
        System.out.println("getSearchTreeRoot");
        ProbPCELOE instance = new ProbPCELOE();
        OENode expResult = null;
        OENode result = instance.getSearchTreeRoot();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getMaximumHorizontalExpansion method, of class ProbPCELOE.
     */
    public void testGetMaximumHorizontalExpansion() {
        System.out.println("getMaximumHorizontalExpansion");
        ProbPCELOE instance = new ProbPCELOE();
        int expResult = 0;
        int result = instance.getMaximumHorizontalExpansion();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getMinimumHorizontalExpansion method, of class ProbPCELOE.
     */
    public void testGetMinimumHorizontalExpansion() {
        System.out.println("getMinimumHorizontalExpansion");
        ProbPCELOE instance = new ProbPCELOE();
        int expResult = 0;
        int result = instance.getMinimumHorizontalExpansion();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getClassExpressionTests method, of class ProbPCELOE.
     */
    public void testGetClassExpressionTests() {
        System.out.println("getClassExpressionTests");
        ProbPCELOE instance = new ProbPCELOE();
        int expResult = 0;
        int result = instance.getClassExpressionTests();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getOperator method, of class ProbPCELOE.
     */
    public void testGetOperator() {
        System.out.println("getOperator");
        ProbPCELOE instance = new ProbPCELOE();
        RefinementOperator expResult = null;
        RefinementOperator result = instance.getOperator();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setOperator method, of class ProbPCELOE.
     */
    public void testSetOperator() {
        System.out.println("setOperator");
        LengthLimitedRefinementOperator operator = null;
        ProbPCELOE instance = new ProbPCELOE();
        instance.setOperator(operator);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getStartClass method, of class ProbPCELOE.
     */
    public void testGetStartClass() {
        System.out.println("getStartClass");
        ProbPCELOE instance = new ProbPCELOE();
        Description expResult = null;
        Description result = instance.getStartClass();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setStartClass method, of class ProbPCELOE.
     */
    public void testSetStartClass() {
        System.out.println("setStartClass");
        Description startClass = null;
        ProbPCELOE instance = new ProbPCELOE();
        instance.setStartClass(startClass);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAllowedConcepts method, of class ProbPCELOE.
     */
    public void testGetAllowedConcepts() {
        System.out.println("getAllowedConcepts");
        ProbPCELOE instance = new ProbPCELOE();
        Set<NamedClass> expResult = null;
        Set<NamedClass> result = instance.getAllowedConcepts();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setAllowedConcepts method, of class ProbPCELOE.
     */
    public void testSetAllowedConcepts() {
        System.out.println("setAllowedConcepts");
        Set<NamedClass> allowedConcepts = null;
        ProbPCELOE instance = new ProbPCELOE();
        instance.setAllowedConcepts(allowedConcepts);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getIgnoredConcepts method, of class ProbPCELOE.
     */
    public void testGetIgnoredConcepts() {
        System.out.println("getIgnoredConcepts");
        ProbPCELOE instance = new ProbPCELOE();
        Set<NamedClass> expResult = null;
        Set<NamedClass> result = instance.getIgnoredConcepts();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setIgnoredConcepts method, of class ProbPCELOE.
     */
    public void testSetIgnoredConcepts() {
        System.out.println("setIgnoredConcepts");
        Set<NamedClass> ignoredConcepts = null;
        ProbPCELOE instance = new ProbPCELOE();
        instance.setIgnoredConcepts(ignoredConcepts);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of isWriteSearchTree method, of class ProbPCELOE.
     */
    public void testIsWriteSearchTree() {
        System.out.println("isWriteSearchTree");
        ProbPCELOE instance = new ProbPCELOE();
        boolean expResult = false;
        boolean result = instance.isWriteSearchTree();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setWriteSearchTree method, of class ProbPCELOE.
     */
    public void testSetWriteSearchTree() {
        System.out.println("setWriteSearchTree");
        boolean writeSearchTree = false;
        ProbPCELOE instance = new ProbPCELOE();
        instance.setWriteSearchTree(writeSearchTree);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSearchTreeFile method, of class ProbPCELOE.
     */
    public void testGetSearchTreeFile() {
        System.out.println("getSearchTreeFile");
        ProbPCELOE instance = new ProbPCELOE();
        String expResult = "";
        String result = instance.getSearchTreeFile();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setSearchTreeFile method, of class ProbPCELOE.
     */
    public void testSetSearchTreeFile() {
        System.out.println("setSearchTreeFile");
        String searchTreeFile = "";
        ProbPCELOE instance = new ProbPCELOE();
        instance.setSearchTreeFile(searchTreeFile);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getMaxNrOfResults method, of class ProbPCELOE.
     */
    public void testGetMaxNrOfResults() {
        System.out.println("getMaxNrOfResults");
        ProbPCELOE instance = new ProbPCELOE();
        int expResult = 0;
        int result = instance.getMaxNrOfResults();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setMaxNrOfResults method, of class ProbPCELOE.
     */
    public void testSetMaxNrOfResults() {
        System.out.println("setMaxNrOfResults");
        int maxNrOfResults = 0;
        ProbPCELOE instance = new ProbPCELOE();
        instance.setMaxNrOfResults(maxNrOfResults);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getNoisePercentage method, of class ProbPCELOE.
     */
    public void testGetNoisePercentage() {
        System.out.println("getNoisePercentage");
        ProbPCELOE instance = new ProbPCELOE();
        double expResult = 0.0;
        double result = instance.getNoisePercentage();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setNoisePercentage method, of class ProbPCELOE.
     */
    public void testSetNoisePercentage() {
        System.out.println("setNoisePercentage");
        double noisePercentage = 0.0;
        ProbPCELOE instance = new ProbPCELOE();
        instance.setNoisePercentage(noisePercentage);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of isFilterDescriptionsFollowingFromKB method, of class ProbPCELOE.
     */
    public void testIsFilterDescriptionsFollowingFromKB() {
        System.out.println("isFilterDescriptionsFollowingFromKB");
        ProbPCELOE instance = new ProbPCELOE();
        boolean expResult = false;
        boolean result = instance.isFilterDescriptionsFollowingFromKB();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setFilterDescriptionsFollowingFromKB method, of class ProbPCELOE.
     */
    public void testSetFilterDescriptionsFollowingFromKB() {
        System.out.println("setFilterDescriptionsFollowingFromKB");
        boolean filterDescriptionsFollowingFromKB = false;
        ProbPCELOE instance = new ProbPCELOE();
        instance.setFilterDescriptionsFollowingFromKB(filterDescriptionsFollowingFromKB);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of isReplaceSearchTree method, of class ProbPCELOE.
     */
    public void testIsReplaceSearchTree() {
        System.out.println("isReplaceSearchTree");
        ProbPCELOE instance = new ProbPCELOE();
        boolean expResult = false;
        boolean result = instance.isReplaceSearchTree();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setReplaceSearchTree method, of class ProbPCELOE.
     */
    public void testSetReplaceSearchTree() {
        System.out.println("setReplaceSearchTree");
        boolean replaceSearchTree = false;
        ProbPCELOE instance = new ProbPCELOE();
        instance.setReplaceSearchTree(replaceSearchTree);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getMaxClassDescriptionTests method, of class ProbPCELOE.
     */
    public void testGetMaxClassDescriptionTests() {
        System.out.println("getMaxClassDescriptionTests");
        ProbPCELOE instance = new ProbPCELOE();
        int expResult = 0;
        int result = instance.getMaxClassDescriptionTests();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setMaxClassDescriptionTests method, of class ProbPCELOE.
     */
    public void testSetMaxClassDescriptionTests() {
        System.out.println("setMaxClassDescriptionTests");
        int maxClassDescriptionTests = 0;
        ProbPCELOE instance = new ProbPCELOE();
        instance.setMaxClassDescriptionTests(maxClassDescriptionTests);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getMaxExecutionTimeInSeconds method, of class ProbPCELOE.
     */
    public void testGetMaxExecutionTimeInSeconds() {
        System.out.println("getMaxExecutionTimeInSeconds");
        ProbPCELOE instance = new ProbPCELOE();
        int expResult = 0;
        int result = instance.getMaxExecutionTimeInSeconds();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setMaxExecutionTimeInSeconds method, of class ProbPCELOE.
     */
    public void testSetMaxExecutionTimeInSeconds() {
        System.out.println("setMaxExecutionTimeInSeconds");
        int maxExecutionTimeInSeconds = 0;
        ProbPCELOE instance = new ProbPCELOE();
        instance.setMaxExecutionTimeInSeconds(maxExecutionTimeInSeconds);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of isTerminateOnNoiseReached method, of class ProbPCELOE.
     */
    public void testIsTerminateOnNoiseReached() {
        System.out.println("isTerminateOnNoiseReached");
        ProbPCELOE instance = new ProbPCELOE();
        boolean expResult = false;
        boolean result = instance.isTerminateOnNoiseReached();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setTerminateOnNoiseReached method, of class ProbPCELOE.
     */
    public void testSetTerminateOnNoiseReached() {
        System.out.println("setTerminateOnNoiseReached");
        boolean terminateOnNoiseReached = false;
        ProbPCELOE instance = new ProbPCELOE();
        instance.setTerminateOnNoiseReached(terminateOnNoiseReached);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of isReuseExistingDescription method, of class ProbPCELOE.
     */
    public void testIsReuseExistingDescription() {
        System.out.println("isReuseExistingDescription");
        ProbPCELOE instance = new ProbPCELOE();
        boolean expResult = false;
        boolean result = instance.isReuseExistingDescription();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setReuseExistingDescription method, of class ProbPCELOE.
     */
    public void testSetReuseExistingDescription() {
        System.out.println("setReuseExistingDescription");
        boolean reuseExistingDescription = false;
        ProbPCELOE instance = new ProbPCELOE();
        instance.setReuseExistingDescription(reuseExistingDescription);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of isUseMinimizer method, of class ProbPCELOE.
     */
    public void testIsUseMinimizer() {
        System.out.println("isUseMinimizer");
        ProbPCELOE instance = new ProbPCELOE();
        boolean expResult = false;
        boolean result = instance.isUseMinimizer();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setUseMinimizer method, of class ProbPCELOE.
     */
    public void testSetUseMinimizer() {
        System.out.println("setUseMinimizer");
        boolean useMinimizer = false;
        ProbPCELOE instance = new ProbPCELOE();
        instance.setUseMinimizer(useMinimizer);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getHeuristic method, of class ProbPCELOE.
     */
    public void testGetHeuristic() {
        System.out.println("getHeuristic");
        ProbPCELOE instance = new ProbPCELOE();
        OEHeuristicRuntime expResult = null;
        OEHeuristicRuntime result = instance.getHeuristic();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setHeuristic method, of class ProbPCELOE.
     */
    public void testSetHeuristic() {
        System.out.println("setHeuristic");
        OEHeuristicRuntime heuristic = null;
        ProbPCELOE instance = new ProbPCELOE();
        instance.setHeuristic(heuristic);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getMaxClassExpressionTestsWithoutImprovement method, of class ProbPCELOE.
     */
    public void testGetMaxClassExpressionTestsWithoutImprovement() {
        System.out.println("getMaxClassExpressionTestsWithoutImprovement");
        ProbPCELOE instance = new ProbPCELOE();
        int expResult = 0;
        int result = instance.getMaxClassExpressionTestsWithoutImprovement();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setMaxClassExpressionTestsWithoutImprovement method, of class ProbPCELOE.
     */
    public void testSetMaxClassExpressionTestsWithoutImprovement() {
        System.out.println("setMaxClassExpressionTestsWithoutImprovement");
        int maxClassExpressionTestsWithoutImprovement = 0;
        ProbPCELOE instance = new ProbPCELOE();
        instance.setMaxClassExpressionTestsWithoutImprovement(maxClassExpressionTestsWithoutImprovement);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getMaxExecutionTimeInSecondsAfterImprovement method, of class ProbPCELOE.
     */
    public void testGetMaxExecutionTimeInSecondsAfterImprovement() {
        System.out.println("getMaxExecutionTimeInSecondsAfterImprovement");
        ProbPCELOE instance = new ProbPCELOE();
        int expResult = 0;
        int result = instance.getMaxExecutionTimeInSecondsAfterImprovement();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setMaxExecutionTimeInSecondsAfterImprovement method, of class ProbPCELOE.
     */
    public void testSetMaxExecutionTimeInSecondsAfterImprovement() {
        System.out.println("setMaxExecutionTimeInSecondsAfterImprovement");
        int maxExecutionTimeInSecondsAfterImprovement = 0;
        ProbPCELOE instance = new ProbPCELOE();
        instance.setMaxExecutionTimeInSecondsAfterImprovement(maxExecutionTimeInSecondsAfterImprovement);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getNrOfThreads method, of class ProbPCELOE.
     */
    public void testGetNrOfThreads() {
        System.out.println("getNrOfThreads");
        ProbPCELOE instance = new ProbPCELOE();
        int expResult = 0;
        int result = instance.getNrOfThreads();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setNrOfThreads method, of class ProbPCELOE.
     */
    public void testSetNrOfThreads() {
        System.out.println("setNrOfThreads");
        int nrOfThreads = 0;
        ProbPCELOE instance = new ProbPCELOE();
        instance.setNrOfThreads(nrOfThreads);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of main method, of class ProbPCELOE.
     */
    public void testMain() throws Exception {
        System.out.println("main");
        String[] args = null;
        ProbPCELOE.main(args);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}

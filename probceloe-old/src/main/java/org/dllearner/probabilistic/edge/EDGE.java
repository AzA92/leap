/**
 *  This file is part of LEAP.
 * 
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, 
 *  but some components can be used as stand-alone.
 * 
 *  LEAP is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  LEAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.dllearner.probabilistic.edge;

import unife.edgeRemote.EDGERemote;
import unife.edgeRemote.EDGERemoteServer;
import unife.edge.EDGEStat;
import unife.edgeRemote.exceptions.InconsistencyException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import org.apache.log4j.Logger;
import org.coode.owlapi.manchesterowlsyntax.ManchesterOWLSyntaxEditorParser;
import org.dllearner.core.AbstractKnowledgeSource;
import org.dllearner.core.AbstractLearningProblem;
import org.dllearner.core.AbstractReasonerComponent;
import org.dllearner.core.ComponentAnn;
import org.dllearner.core.ComponentInitException;
import org.dllearner.core.KnowledgeSource;
import org.dllearner.core.LearningProblemUnsupportedException;
import org.dllearner.core.config.ConfigOption;
import org.dllearner.core.owl.Axiom;
import org.dllearner.core.owl.Description;
import org.dllearner.core.owl.Individual;
import org.dllearner.probabilistic.core.AbstractLearningParameter;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.dllearner.core.owl.NamedClass;
import org.dllearner.kb.OWLFile;
import org.dllearner.kb.OWLOntologyKnowledgeSource;
import org.dllearner.learningproblems.ClassLearningProblem;
import org.dllearner.learningproblems.PosNegLP;
import org.dllearner.learningproblems.PosOnlyLP;
import org.dllearner.probabilistic.core.ParameterLearnerException;
import org.dllearner.reasoning.FastInstanceChecker;
import org.dllearner.utilities.Helper;
import org.dllearner.utilities.RMIUtils;
import org.dllearner.utilities.ReflectionHelper;
import org.dllearner.utilities.owl.OWLAPIAxiomConvertVisitor;
import org.mindswap.pellet.utils.Timer;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.expression.ParserException;
import org.semanticweb.owlapi.io.OWLFunctionalSyntaxOntologyFormat;
import org.semanticweb.owlapi.io.OWLXMLOntologyFormat;
import org.semanticweb.owlapi.io.RDFXMLOntologyFormat;
import org.semanticweb.owlapi.model.AddImport;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLImportsDeclaration;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyChange;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyFormat;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.util.OWLEntityRemover;
import uk.ac.manchester.cs.owl.owlapi.OWLDataFactoryImpl;

/**
 * The EDGE (Em over bDds for description loGics paramEter learner). This class
 * use RMI (Remote method invocations) to the EDGE application written by the
 * unife group, so to make this class work the EDGE server must be launched.
 *
 * @author Giuseppe Cota <giuseta@gmail.com>
 */
@ComponentAnn(name = "EDGE", shortName = "edge", version = 0.2)
public class EDGE extends AbstractLearningParameter {

    private OWLOntologyManager manager;

    private static Logger logger = Logger.getLogger(EDGE.class);

    private EDGERemote eDGERemote;

    private EDGERemoteServer eDGEServer;

    private EDGEStat currentStat = null;
    private EDGEStat previousStat = null;

    //OWLOntology completeLearnedOntology; // final complete learned ontology
    OWLOntology completeOntology; // starting complete ontology

    //private NamedClass classToDescribe = null;
//    // don't need to set up the ontology because it merges all the sources and create a new file
//    @ConfigOption(name="ontology", "URI of file with the ontology", null, true, true);
//        options.add(new StringConfigOption("positiveExamples", "URI of file with positive examples"));
    private Set<Individual> positiveIndividuals;
    //options.add(new StringConfigOption("negativeExamples", "URI of file with positive examples"));
    private Set<Individual> negativeIndividuals;

    @ConfigOption(name = "seed", description = "seed for random generation", defaultValue = "0")
    private int seed = 0;

    @ConfigOption(name = "randomize", description = "randomize the starting probabilities of the probabilistic axioms", defaultValue = "false")
    private boolean randomize = false;

    // se randomize All � true allora anche randomize � true
    @ConfigOption(name = "randomizeAll", description = "randomize all the axioms in the starting probabilistic ontology (including non probabilistic ones)", defaultValue = "false")
    private boolean randomizeAll = false;

    @ConfigOption(name = "differenceLL", description = "stop difference between log-likelihood of two consecutive EM cycles", defaultValue = "0.000000000028")
    private double differenceLL = 0.000000000028;

    @ConfigOption(name = "ratioLL", description = "stop ratio between log-likelihood of two consecutive EM cycles", defaultValue = "0.000000000028")
    private double ratioLL = 0.000000000028;

    @ConfigOption(name = "maxIterations", description = "maximum number of cycles", defaultValue = "2147000000")
    private long maxIterations = 2147000000L;

    @ConfigOption(name = "maxExplanations", description = "the maximum number of explanations to find for each query", defaultValue = "" + Integer.MAX_VALUE)
    private int maxExplanations = Integer.MAX_VALUE;
    // ATTENZIONE EDGE USA IL FORMATO STRING PER TIMEOUT!!!        
    @ConfigOption(name = "timeout", description = "max time allowed for the inference", defaultValue = "0 (infinite timeout)")
    private int timeout = 0;

    @ConfigOption(name = "showAll", description = "force the visualization of all results", defaultValue = "false")
    private boolean showAll = false;

    @ConfigOption(name = "outputFileFormat", description = "format of the output file", defaultValue = "OWLXML")
    private EDGERemote.PossibleOutputFileFormat outputFileFormat = EDGERemote.PossibleOutputFileFormat.OWLXML;

//    possibleFormat.setAllowedValues ( new String[]{"OWLXML"
//    , "OWLFUNCTIONAL", "RDFXML"});
//        options.add (possibleFormat);
    @ConfigOption(name = "severAddress", description = "A string in format <host>:<port> or only <host>, if only the host has been defined then the port will be 1099", defaultValue = "localhost:1099")
    private String serverAddress = "localhost:1099";

    @ConfigOption(name = "positiveFile", description = "Output file containing the positive examples", defaultValue = "positiveExamples.owl")
    private String positiveFile = "positiveExamples.owl";

    @ConfigOption(name = "negativeFile", description = "Output file containing the negative examples", defaultValue = "negativeExamples.owl")
    private String negativeFile = "negativeExamples.owl";

    @ConfigOption(name = "maxPositiveExamples", description = "max number of positive examples that edge must handle when a class learning problem is given", defaultValue = "0 (infinite)")
    private int maxPositiveExamples = 0;

    @ConfigOption(name = "maxNegativeExamples", description = "max number of negative examples that edge must handle when a class learning problem is given", defaultValue = "0 (infinite)")
    private int maxNegativeExamples = 0;

    @ConfigOption(name = "keepParameters", description = "If true EDGE keeps the old parameter values of all the probabilistic axioms and it does not relearn them", defaultValue = "false")
    private boolean keepParameters = false;

    /**
     * ****************************************************************
     */
    /* Hidden options **************************************************/
    @ConfigOption(name = "outputFile", description = "output file containing only the learned probabilistic axioms", defaultValue = "<currentDir>/probs.owl")
    private String outputFile = System.getProperty("user.dir") + "/probs.owl";

    /**
     * ****************************************************************
     */
    //@ConfigOption(name = "accuracyThreshold", description = "the axiom under the accuracy threshold are not considered by EDGE", defaultValue = "0")
    //private double accuracyThreshold = 0.0;
    //@ConfigOption(name = "initialOntologyFile", description = "File conatining all the sources merged", defaultValue = "tmp/complete_ontology.owl")
    //private String initialOntologyFile = "tmp/complete_ontology.owl";
    private Map<String, Long> timeMap = new HashMap<String, Long>();

    public EDGE() {

    }

    public EDGE(AbstractLearningProblem learningProblem, Set<Axiom> targetAxioms) {
        super(learningProblem, targetAxioms);
    }
    /*
     public EDGE(String ontology, String targetAxioms, String positiveExamples, String negativeExamples){
        
     throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
     }
     */

    @Override
    public double getParameter(Axiom ax) throws ParameterLearnerException {
        if (completeOntology == null || currentStat == null) {
            String message;
            if (completeOntology == null) {
                message = "No ontology setted yet, you have to initialize the class first (call init()) ";
            } else {
                message = "No parameter learned yet";
            }
            logger.error(message);
            throw new ParameterLearnerException(message);
        }
        // convert from DLLearner axiom to OWLAPI axiom
        OWLAxiom axiom = OWLAPIAxiomConvertVisitor.convertAxiom(ax);

        for (OWLAxiom returnedAxiom : currentStat.getProbAxioms().keySet()) {
            if (returnedAxiom.equalsIgnoreAnnotations(axiom)) {
                return currentStat.getProbAxioms().get(returnedAxiom);
            }
        }
        String message = "Axiom not found among the returned ones";
        logger.error(message);
        throw new ParameterLearnerException(message);
    }

    @Override
    public double getLL() {
        if (currentStat == null) {
            return Double.NEGATIVE_INFINITY;
        }
        return currentStat.getLL();
    }

    @Override
    public Map<String, Long> getTimeMap() {
        return timeMap;
    }

    @Override
    public void init() throws ComponentInitException {
        //AnnComponentManager.setComponentClassNames(componentClassNames);
        logger.info("Initializing EDGE");
        // initializing time map
        //timeMap.put("EDGE", 0L);
        timeMap.put("Bundle", 0L);
        timeMap.put("EM", 0L);
        timeMap.put("PMap", 0L);
        timeMap.put("Other", 0L);
        // create OWL API ontology manager - make sure we use a new data factory so that we don't default to the static one which can cause problems in a multi threaded environment.
        manager = OWLManager.createOWLOntologyManager(new OWLDataFactoryImpl());
        sources = learningProblem.getReasoner().getSources();
        if (learningProblem == null) {
            logger.info("Error: learning problem is null");
        }
        completeOntology = createOntology();

        // set codebase
        if (System.getProperty("java.rmi.server.codebase") == null) {
            String codebase = EDGE.class.getProtectionDomain().getCodeSource().getLocation().toString();
            System.setProperty("java.rmi.server.codebase", codebase);
            logger.debug("Setted default codebase: " + codebase);
        }

        System.setProperty("java.rmi.server.useCodebaseOnly", "false");

        // create a policy file
        // If java.security.policy option is not setted, a default file will be created
        if (System.getProperty("java.security.policy") == null) {
            String path = createPolicyFile();
            System.setProperty("java.security.policy", path);
            logger.debug("Setted default file policy" + logger);
        }
        // create Security manager
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }

        // search in the RMI registry a remote class named EDGERemoteFactory
        String edgeFactory = EDGERemoteServer.class.getSimpleName();
        String ipServer;
        int port = 1099;
        serverAddress = serverAddress.trim();
        if (!isValidAddress(serverAddress)) {
            String message = "Invalid server address: " + serverAddress;
            logger.error(message);
            throw new ComponentInitException(message);
        }
        if (serverAddress.indexOf(":") != -1) { // host and port are defined
            String[] address = serverAddress.split(":", 2);
            ipServer = address[0];
            port = Integer.parseInt(address[1]);
        } else { // only the host is defined
            ipServer = serverAddress;
        }
        try {
            Registry registry = LocateRegistry.getRegistry(ipServer, port);
            eDGEServer = (EDGERemoteServer) registry.lookup(edgeFactory);
            // create remote class
            eDGERemote = eDGEServer.createEDGE(); // devo passare degli argomenti?
        } catch (RemoteException ex) {
            logger.error(ex.getMessage());
            throw new ComponentInitException(ex.getMessage());
        } catch (NotBoundException ex) {
            logger.error(ex.getMessage());
            throw new ComponentInitException("Not Bound Exception: " + ex.getMessage());
        }

        // setting phase
        try {
            if (learningProblem instanceof PosNegLP) {
                positiveIndividuals = ((PosNegLP) learningProblem).getPositiveExamples();
                negativeIndividuals = ((PosNegLP) learningProblem).getNegativeExamples();
            } else if (learningProblem instanceof PosOnlyLP) {
                positiveIndividuals = ((PosOnlyLP) learningProblem).getPositiveExamples();
                // use pseudo-negative individuals
                negativeIndividuals = Helper.difference(learningProblem.getReasoner().getIndividuals(), positiveIndividuals);
            } else if (learningProblem instanceof ClassLearningProblem) {
                eDGERemote.setConceptToDescribe(((ClassLearningProblem) learningProblem).getClassToDescribe());
                // Java Reflection has been used to get values from private fields. 
                //It's neither a conventional way nor the universally suggested idea,
                // but in this case is the only way to extract positive and negative individuals
                // without modifing the DLLearner code (the creation of a plugin is the objective)
                //positiveIndividuals = ReflectionHelper.getPrivateField(learningProblem, "classInstancesSet");
                List positiveIndividualsList = ReflectionHelper.getPrivateField(learningProblem, "classInstances");
                if (maxPositiveExamples > 0) {
                    //List positiveIndividualsList = new ArrayList(positiveIndividuals);
                    Collections.shuffle(positiveIndividualsList);
                    if (maxPositiveExamples < positiveIndividualsList.size()) {
                        positiveIndividualsList = positiveIndividualsList.subList(0, maxPositiveExamples);
                    }
                }
                positiveIndividuals = new HashSet(positiveIndividualsList);
                // instances of super classes excluding instances of the class itself
                List<Individual> negativeIndividualsList = ReflectionHelper.getPrivateField(learningProblem, "superClassInstances");
                if (maxNegativeExamples > 0) {
                    Collections.shuffle(negativeIndividualsList);
                    if (maxNegativeExamples < negativeIndividualsList.size()) {
                        negativeIndividualsList = negativeIndividualsList.subList(0, maxNegativeExamples);
                    }
                }
                negativeIndividuals = new TreeSet(negativeIndividualsList);
            } else {
                try {
                    throw new LearningProblemUnsupportedException(learningProblem.getClass(), this.getClass());
                } catch (LearningProblemUnsupportedException e) {
                    throw new ComponentInitException(e.getMessage());
                }
            }

            // set positive individuals
            Set<String> positiveIndividualsString = new TreeSet<String>();
            for (Individual ind : positiveIndividuals) {
                positiveIndividualsString.add(ind.getName());
            }
            eDGERemote.setPositiveExamples(positiveIndividualsString);

            // set negative individuals
            Set<String> negativeIndividualsString = new TreeSet<String>();
            for (Individual ind : negativeIndividuals) {
                negativeIndividualsString.add(ind.getName());
            }
            eDGERemote.setNegativeExamples(negativeIndividualsString);

            //eDGERemote.setKeepParameters(keepParameters);
            if (reduceOntology) {
                reduceOntology(completeOntology);
            }
//            if (ipServer.equals("localhost") || ipServer.substring(0, 4).equals("127.")) {
//                String completeOntologyPath = saveCreatedOntology(completeOntology);
//                eDGERemote.setOntology(completeOntologyPath);
//                completeOntology = null;
//            } else {
//                // per ora fai la stessa cosa in locale
//                //String completeOntologyPath = saveCreatedOntology(completeOntology);
//                //eDGERemote.setOntology(completeOntologyPath);
//                throw new UnsupportedOperationException("Operation not supported yet");
//            }
            String completeOntologyPath = saveCreatedOntology(completeOntology);
            completeOntology = null;
            File upload = new File(completeOntologyPath);
            RMIUtils.upload(eDGEServer, upload);
            eDGERemote.setOntology(upload);
            eDGERemote.setDifferenceLL(differenceLL);
            eDGERemote.setMaxExplanations(maxExplanations);
            eDGERemote.setMaxIterations(maxIterations);
            eDGERemote.setOutputFileFormat(outputFileFormat);
            eDGERemote.setRandomize(randomize);
            eDGERemote.setRandomizeAll(randomizeAll);
            eDGERemote.setRatioLL(ratioLL);
            eDGERemote.setSeed(seed);
            eDGERemote.setShowAll(showAll);
            eDGERemote.setTimeout(timeout);
        } catch (Exception e) {
            logger.error(e);
            logger.error(e.getMessage());
            if (e.getCause() != null) {
                logger.error(e.getCause().getMessage());
            }
            try {
                exit();
            } catch (RemoteException ex) {
                logger.error("Unable to kill the server");
            }
            throw new ComponentInitException(e.getMessage());
        }
    }

    public static String getName() {
        return "EDGE";
    }

    /**
     * Execute test without parameter learning.
     */
    public void test() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Create a temporary policy file.
     *
     * @return the absolute path of the temporary file
     */
    private String createPolicyFile() {
        //String str = " grant codeBase \"" + EDGE.class
        //        .getProtectionDomain().getCodeSource().getLocation().toString() + "\" {\n"
        // ONLY FOR TEST    
        String str = " grant {\n"
                + "\tpermission java.security.AllPermission; \n"
                + "};";
        File policyFile = null;

        try {
            policyFile = File.createTempFile("edgeClient", ".policy");
            policyFile.deleteOnExit();
        } catch (IOException ex) {
            logger.fatal("Fatal error creating the temporary policy file", ex);
            System.exit(-1);
        }

        try {
            PrintWriter writer = new PrintWriter(new FileWriter(policyFile));
            writer.println(str);
            writer.close();
        } catch (IOException ex) {
            System.out.println("Error writing policy file. See log for more details");
            logger.fatal(ex.getMessage());
            System.exit(-1);
        }
        //}

        return policyFile.getAbsolutePath();
    }

    /**
     * This method merges all the input knowledge sources and returns the
     * filename of the new ontology. This method is not very robust, hence its
     * call should be avoided. This method takes from the learning problem the
     * sources, merge them and save the resulting ontology in a temporary file.
     * Finally it returns the absolute path of the ontology file saved in OWLXML
     * format
     *
     * @return Filename of the new ontology.
     */
    private OWLOntology createOntology() throws ComponentInitException {
        logger.info("Number of sources: " + sources.size());
        logger.info("creating ontology through merging of the sources");
        // list of source ontologies
        List<OWLOntology> owlAPIOntologies = new LinkedList<OWLOntology>();
        // used prefix
        Map<String, String> prefixes = new TreeMap<String, String>();

        Set<OWLImportsDeclaration> directImports = new HashSet<OWLImportsDeclaration>();

        for (KnowledgeSource source : sources) {
            OWLOntology ontology;
            if (source instanceof OWLOntologyKnowledgeSource) {
                ontology = ((OWLOntologyKnowledgeSource) source).createOWLOntology(manager);
                owlAPIOntologies.add(ontology);
            } else {
                //This parameter learner requires an ontology to process
                String message = "EDGE Parameter Learner Requires an OWLKnowledgeSource.  Received a KS of type: " + source.getClass().getName();
                logger.error(message);
                throw new ComponentInitException(message);
            }

            directImports.addAll(ontology.getImportsDeclarations());
        }

        //Now merge all of the knowledge sources into one ontology instance.
        try {
            logger.info("Merging the ontologies...");
            //The following line illustrates a problem with using different OWLOntologyManagers.  This can manifest itself if we have multiple sources who were created with different manager instances.
            //ontology = OWLManager.createOWLOntologyManager().createOntology(IRI.create("http://dl-learner/all"), new HashSet<OWLOntology>(owlAPIOntologies));
            OWLOntology allOntology = manager.createOntology(IRI.create("http://dl-learner/all"), new HashSet<OWLOntology>(owlAPIOntologies));
            //we have to add all import declarations manually here, because this are no axioms
            List<OWLOntologyChange> addImports = new ArrayList<OWLOntologyChange>();
            for (OWLImportsDeclaration i : directImports) {
                addImports.add(new AddImport(allOntology, i));
            }
            manager.applyChanges(addImports);
            logger.info("Ontologies merged. Complete ontology created");
            return allOntology;
        } catch (OWLOntologyCreationException e1) {
            String message = "OWLOntologyCreationException complete ontology NOT created. ";
            logger.error(message + e1.getMessage());
            throw new ComponentInitException(message);
        }

//        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
//        OWLOntology ontology;
//        Set<OWLOntology> owlAPIOntologies = new HashSet<OWLOntology>();
//        for (KnowledgeSource source : sources) {
//
//            if (source instanceof OWLFile || source instanceof SparqlKnowledgeSource || source instanceof OWLAPIOntology) {
//                URL url = null;
//                if (source instanceof OWLFile) {
//                    url = ((OWLFile) source).getURL();
//                }
//
//                try {
//                    if (source instanceof OWLAPIOntology) {
//                        ontology = ((OWLAPIOntology) source).getOWLOntolgy();
//                    } else if (source instanceof SparqlKnowledgeSource) {
//                        ontology = ((SparqlKnowledgeSource) source).getOWLAPIOntology();
//                        manager = ontology.getOWLOntologyManager();
//                    } else {
//                        ontology = manager.loadOntologyFromOntologyDocument(IRI.create(url.toURI()));
//                    }
//
//                    owlAPIOntologies.add(ontology);
//
//                } catch (OWLOntologyCreationException e) {
//                    e.printStackTrace();
//                } catch (URISyntaxException e) {
//                    e.printStackTrace();
//                }
//                // all other sources are converted to KB and then to an
//                // OWL API ontology
//            } else {
//                KB kb = source.toKB();
////				System.out.println(kb.toString(null,null));
//
//                IRI ontologyURI = IRI.create("http://example.com");
//                ontology = null;
//                try {
//                    ontology = manager.createOntology(ontologyURI);
//                } catch (OWLOntologyCreationException e) {
//                    e.printStackTrace();
//                }
//                OWLAPIAxiomConvertVisitor.fillOWLOntology(manager, ontology, kb);
//                owlAPIOntologies.add(ontology);
//            }
//        }
//        try {
//            ontology = manager.createOntology(IRI.create("http://dl-learner/all"), new HashSet<OWLOntology>(owlAPIOntologies));
//            File ontFile = File.createTempFile("all_ontologies", ".owl");
//            OWLOntologyFormat format = manager.getOntologyFormat(ontology);
//            // save ontology in OWLXML format
//            OWLXMLOntologyFormat owlxmlFormat = new OWLXMLOntologyFormat();
//            if (format.isPrefixOWLOntologyFormat()) {
//                owlxmlFormat.copyPrefixesFrom(format.asPrefixOWLOntologyFormat());
//            }
//            manager.saveOntology(ontology, owlxmlFormat, IRI.create(ontFile.toURI()));
//            return ontFile.getAbsolutePath();
//        } catch (OWLOntologyCreationException e1) {
//            e1.printStackTrace();
//        }
    }

    @Override
    public void run() {
        try {
            currentStat = eDGERemote.computeLearning();
            Map<String, Long> timers = currentStat.getTimers();
            timeMap.put("Bundle", timeMap.get("Bundle") + timers.get("Bundle"));
            timeMap.put("EM", timeMap.get("EM") + timers.get("EM"));
            timeMap.put("PMap", timeMap.get("PMap") + timers.get("PMap"));
        } catch (RemoteException e) {
            logger.fatal(e.getMessage());
            try {
                eDGEServer.exit();
            } catch (RemoteException ex) {
                try {
                    eDGEServer.forceExit();
                } catch (RemoteException ex2) {
                }
            } finally {
                System.exit(-1);
            }
        }
    }

//    /**
//     * @param positiveIndividuals the positiveIndividuals to set
//     */
//    public void setPositiveIndividuals(Set<Individual> positiveIndividuals) {
//        this.positiveIndividuals = positiveIndividuals;
//    }
//
//    /**
//     * @param negativeIndividuals the negativeIndividuals to set
//     */
//    public void setNegativeIndividuals(Set<Individual> negativeIndividuals) {
//        this.negativeIndividuals = negativeIndividuals;
//    }
    /**
     * @return the outputFile
     */
    public String getOutputFile() {
        return outputFile;
    }

    /**
     * @param outputFile the outputFile to set
     */
    public void setOutputFile(String outputFile) {
        this.outputFile = outputFile;
    }

    /**
     * @return the seed
     */
    public int getSeed() {
        return seed;
    }

    /**
     * @param seed the seed to set
     */
    public void setSeed(int seed) {
        this.seed = seed;
    }

    /**
     * @return the randomize
     */
    public boolean isRandomize() {
        return randomize;
    }

    /**
     * @param randomize the randomize to set
     */
    public void setRandomize(boolean randomize) {
        this.randomize = randomize;
    }

    /**
     * @return the randomizeAll
     */
    public boolean isRandomizeAll() {
        return randomizeAll;
    }

    /**
     * @param randomizeAll the randomizeAll to set
     */
    public void setRandomizeAll(boolean randomizeAll) {
        this.randomizeAll = randomizeAll;
    }

    /**
     * @return the differenceLL
     */
    public double getDifferenceLL() {
        return differenceLL;
    }

    /**
     * @param differenceLL the differenceLL to set
     */
    public void setDifferenceLL(double differenceLL) {
        if (differenceLL <= 0) {
            String message = "Wrong Log-Likelihood difference value: " + differenceLL + ".\nIt must be a real "
                    + "number greater that 0. Default value will be used";
            logger.error(message);
            return;
        }
        this.differenceLL = differenceLL;
    }

    /**
     * @return the ratioLL
     */
    public double getRatioLL() {
        return ratioLL;
    }

    /**
     * @param ratioLL the ratioLL to set
     */
    public void setRatioLL(double ratioLL) {
        if (ratioLL <= 0 || ratioLL > 1) {
            String message = "Wrong ratio value: " + ratioLL + ".\nIt must be a real "
                    + "number greater that 0 and less or equal than 1. Default value will be used";
            logger.error(message);
            return;
        }
        this.ratioLL = ratioLL;
    }

    /**
     * @return the maxIterations
     */
    public long getMaxIterations() {
        return maxIterations;
    }

    /**
     * @param maxIterations the maxIterations to set
     */
    public void setMaxIterations(long maxIterations) {
        if (maxIterations <= 0) {
            String message = "Wrong max number of iterations: " + maxIterations + ".\nIt must be a natural "
                    + "number greater that 0. Default value: " + this.maxIterations + " will be used";
            logger.error(message);
            return;
        }
        this.maxIterations = maxIterations;
    }

    /**
     * @return the maxExplanations
     */
    public int getMaxExplanations() {
        return maxExplanations;
    }

    /**
     * @param maxExplanations the maxExplanations to set
     */
    public void setMaxExplanations(int maxExplanations) {
        if (maxExplanations <= 0) {
            String message = "Wrong man number of explanations: " + maxExplanations + ".\nIt must be a natural "
                    + "number greater that 0. Default value: " + this.maxExplanations + " will be used";
            logger.error(message);
            return;
        }
        this.maxExplanations = maxExplanations;
    }

    /**
     * @return the timeout
     */
    public int getTimeout() {
        return timeout;
    }

    /**
     * @param timeout the timeout to set
     */
    public void setTimeout(int timeout) {
        if (timeout < 0) {
            String message = "Wrong man timeout (in seconds) value: " + timeout + ".\nIt must be an integer "
                    + "number equal or greater that 0 (0 = infinite timeout). No timeout setted";
            logger.error(message);
            return;
        }
        this.timeout = timeout;
    }

    /**
     * @return the showAll
     */
    public boolean isShowAll() {
        return showAll;
    }

    /**
     * @param showAll the showAll to set
     */
    public void setShowAll(boolean showAll) {
        this.showAll = showAll;
    }

    /**
     * @return the outputFileFormat
     */
    public EDGERemote.PossibleOutputFileFormat getOutputFileFormat() {
        return outputFileFormat;
    }

    /**
     * @param outputFileFormat the outputFileFormat to set
     */
    public void setOutputFileFormat(EDGERemote.PossibleOutputFileFormat outputFileFormat) {
        this.outputFileFormat = outputFileFormat;
    }
//    public void setOutputFileFormat(String outputFileFormat) {
//        if (outputFileFormat.equals("RDFXML")) {
//            this.outputFileFormat = EDGERemote.PossibleOutputFileFormat.OWLXML;
//        } else {
//            if (outputFileFormat.equals("OWLFUNCTIONAL")) {
//                this.outputFileFormat = EDGERemote.PossibleOutputFileFormat.OWLFUNCTIONAL;
//            }
//            else { 
//                this.outputFileFormat = EDGERemote.PossibleOutputFileFormat.OWLXML;
//            }
//        }
//
//    }

    /**
     * @return the serverAddress
     */
    public String getServerAddress() {
        return serverAddress;
    }

    /**
     * @param serverAddress the serverAddress to set
     */
    public void setServerAddress(String serverAddress) {
        if (isValidAddress(serverAddress)) {
            this.serverAddress = serverAddress;
        }
    }

    public void exit() throws RemoteException {
        try {
            eDGEServer.exit();
        } catch (RemoteException e) {
            logger.error("Unable to kill the server normally");
            logger.error(e.getMessage());
            if (e.getCause() != null) {
                logger.error(e.getCause().getMessage());
            }
            logger.error("Tryng to kill the server brutally");
            eDGEServer.forceExit();
        }

    }

    private boolean isValidAddress(String value) {
        if (value.isEmpty()) {
            return false;
        }

        if (value.indexOf(":") != -1) { // host and port are defined
            try {
                String[] address = value.split(":", 2);
                InetAddress.getByName(address[0]);
                Integer.parseInt(address[1]);
                return true;
            } catch (Exception e) {
                return false;
            }
        } else { // only the host is defined
            try {
                InetAddress.getByName(value);
                return true;
            } catch (UnknownHostException e) {
                return false;
            }
        }
    }

    @Override
    public void addAxiom(Description classExpression, double epistemicProb) throws ParameterLearnerException, InconsistencyException {
        previousStat = currentStat;
        try {
            eDGERemote.addSubClassOfAxiom(classExpression, epistemicProb);
        } catch (RemoteException e) {
            logger.info(e.getMessage());
            if (e.getCause() != null) {
                logger.info(e.getCause().getMessage());
            }
            throw new ParameterLearnerException(e.getMessage());
        }
    }

    @Override
    public void removeLastAxiom() throws ParameterLearnerException {
        currentStat = previousStat;
        try {
            eDGERemote.removeLastAxiom();
        } catch (RemoteException e) {
            logger.info(e.getMessage());
            if (e.getCause() != null) {
                logger.info(e.getCause().getMessage());
            }
            throw new ParameterLearnerException(e.getMessage());
        }
    }

    @Override
    public void updateOntology() throws ParameterLearnerException {
        try {
            eDGERemote.updateOntology();
        } catch (RemoteException e) {
            logger.info(e.getMessage());
            if (e.getCause() != null) {
                logger.info(e.getCause().getMessage());
            }
            throw new ParameterLearnerException(e.getMessage());
        }
    }

    /**
     * Saves the created ontology by merging all the sources.
     *
     * @param ontology
     * @return
     * @throws org.dllearner.probabilistic.core.ParameterLearnerException
     */
    //This method is used to save the ontology obtained by merging the source ontologies.
    public String saveCreatedOntology(OWLOntology ontology) throws ParameterLearnerException {
        //OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        OWLOntologyFormat format = manager.getOntologyFormat(ontology);
        // save in OWLXML format
        OWLXMLOntologyFormat owlxmlFormat = new OWLXMLOntologyFormat();

        if (format.isPrefixOWLOntologyFormat()) {
            owlxmlFormat.copyPrefixesFrom(format.asPrefixOWLOntologyFormat());
        }
        try {
            File file = File.createTempFile("complete_ontology", ".owl");
            //File file = new File(initialOntologyFile);
            manager.saveOntology(ontology, owlxmlFormat, IRI.create(file.toURI()));
            return file.getAbsolutePath();
        } catch (IOException e) {
            String message = "Error: Unable to create ontology file: " + e.getMessage();
            logger.error(message);
            throw new ParameterLearnerException(message);
        } catch (OWLOntologyStorageException e) {
            String message = "Error: Unable to save ontology file: " + e.getMessage();
            logger.error(message);
            throw new ParameterLearnerException(message);
        }
    }

    public static void main(String[] args) throws ComponentInitException {
        AbstractKnowledgeSource ks = new OWLFile("examples/family/father_oe.owl");
        ks.init();

        AbstractReasonerComponent rc = new FastInstanceChecker(ks);
        rc.init();

        //PosNegLPStandard pnlp = new PosNegLPStandard(rc, positiveIndividuals, negativeIndividuals);
        //PosOnlyLP polp = new PosOnlyLP(rc, positiveIndividuals);
        ClassLearningProblem celp = new ClassLearningProblem(rc);
        celp.setClassToDescribe(new NamedClass("http://example.com/father#father"));
        celp.setCheckConsistency(false);
        celp.init();
        EDGE instance = new EDGE(celp, null);
        instance.init();
    }

    @Override
    public void saveLearnedOntology(File f) throws ParameterLearnerException, IOException {
        saveLearnedOntology(f, false);
    }

    @Override
    public void saveCompleteLearnedOntology(File f) throws ParameterLearnerException, IOException {
        saveLearnedOntology(f, true);
    }

    private void saveLearnedOntology(File f, boolean merge) throws ParameterLearnerException, IOException {
        eDGERemote.setMerge(merge);
        logger.info("Saving the complete learned ontology in " + f.getAbsolutePath());

//        if (serverIsLocal()) {// client and server are in the same machine
//            eDGERemote.saveCompleteLearnedOntology(f.toURI().toURL());
//        } else {// server is remote
        try {
            OWLOntology completeLearnedOntology = eDGERemote.getLearnedOntology();
            manager.saveOntology(completeLearnedOntology, getFormat(), IRI.create(f));
        } catch (Exception e) {
            logger.info("Error saving the complete learned ontology with the first method.");
            logger.info("Trying the second method");
            int BUFFSIZE = 1024;
            InputStream in = eDGERemote.getLearnedOntologyInputStream();
            FileOutputStream out = new FileOutputStream(f);
            byte[] b = new byte[BUFFSIZE];
            int len;
            while ((len = in.read(b)) >= 0) {
                out.write(b, 0, len);
            }
            in.close();
            out.close();
        }
//        }
    }

    @Override
    public void savePositiveExamples() throws ParameterLearnerException, IOException {
        logger.info("Saving positive Examples in " + positiveFile);
        if (serverIsLocal()) { // client and server are in the same machine

        } else { // server is remote

        }
        File file = new File(positiveFile);
        try {
            OWLOntology positiveOntology = eDGERemote.getPositiveExamplesOntology();
            manager.saveOntology(positiveOntology, getFormat(), IRI.create(file));
        } catch (Exception e) {
            logger.info("Error saving the positive examples ontology with the first method.");
            logger.info("Trying the second method");
            int BUFFSIZE = 1024;
            InputStream in = eDGERemote.getPositiveExamplesInputStream();
            FileOutputStream out = new FileOutputStream(file);
            byte[] b = new byte[BUFFSIZE];
            int len;
            while ((len = in.read(b)) >= 0) {
                out.write(b, 0, len);
            }
            in.close();
            out.close();
        }

    }

    @Override
    public void saveNegativeExamples() throws ParameterLearnerException, IOException {
        logger.info("Saving negative Examples in " + negativeFile);
        if (serverIsLocal()) { // client and server are in the same machine

        } else { // server is remote

        }
        File file = new File(negativeFile);
        try {
            OWLOntology negativeOntology = eDGERemote.getNegativeExamplesOntology();
            manager.saveOntology(negativeOntology, getFormat(), IRI.create(new File(negativeFile)));
        } catch (Exception e) {
            logger.info("Error saving the negative examples ontology with the first method.");
            logger.info("Trying the second method");
            int BUFFSIZE = 1024;
            InputStream in = eDGERemote.getNegativeExamplesInputStream();
            FileOutputStream out = new FileOutputStream(file);
            byte[] b = new byte[BUFFSIZE];
            int len;
            while ((len = in.read(b)) >= 0) {
                out.write(b, 0, len);
            }
            in.close();
            out.close();
        }

    }

    private OWLOntologyFormat getFormat() {
        switch (outputFileFormat) {
            case OWLXML:
                return new OWLXMLOntologyFormat();
            case OWLFUNCTIONAL:
                return new OWLFunctionalSyntaxOntologyFormat();
            case RDFXML:
                return new RDFXMLOntologyFormat();
            default:
                return new OWLXMLOntologyFormat();
        }
    }

    private boolean serverIsLocal() {
        String ipServer;
        if (serverAddress.indexOf(":") != -1) { // host and port are defined
            String[] address = serverAddress.split(":", 2);
            ipServer = address[0];
        } else { // only the host is defined
            ipServer = serverAddress;
        }
        return (ipServer.equals("localhost") || ipServer.substring(0, 4).equals("127."));
    }

    /**
     * @param positiveFile the positiveFile to set
     */
    public void setPositiveFile(String positiveFile) {
        this.positiveFile = positiveFile;
    }

    /**
     * @param negativeFile the negativeFile to set
     */
    public void setNegativeFile(String negativeFile) {
        this.negativeFile = negativeFile;
    }

    /**
     * @return the positiveFile
     */
    public String getPositiveFile() {
        return positiveFile;
    }

    /**
     * @return the negativeFile
     */
    public String getNegativeFile() {
        return negativeFile;
    }

    @Override
    public void stop() {
        try {
            exit();
        } catch (RemoteException e) {
            //logger.error("Unable to kill the server");
            logger.error(e.getMessage());
        }
    }

//    /**
//     * @return the initialOntologyFile
//     */
//    public String getInitialOntologyFile() {
//        return initialOntologyFile;
//    }
//
//    /**
//     * @param initialOntologyFile the initialOntologyFile to set
//     */
//    public void setInitialOntologyFile(String initialOntologyFile) {
//        this.initialOntologyFile = initialOntologyFile;
//    }
    private void reduceOntology(OWLOntology ontology) {
        OWLEntityRemover remover = new OWLEntityRemover(manager, Collections.singleton(ontology));
        logger.info("Number of individuals before reduction: " + ontology.getIndividualsInSignature().size());
        logger.info("Reduction at work...");
        boolean delete = true;
        for (OWLNamedIndividual ind : ontology.getIndividualsInSignature()) {
            for (Individual posNegInd : Helper.union(positiveIndividuals, negativeIndividuals)) {
                //logger.info("Example: " + posNegInd.toString().trim());
                if (posNegInd.toString().trim().equals(ind.getIRI().toString().trim())) {
                    //ind.accept(remover);
                    //found = true;
                    delete = false;
                    break;
                }
            }
            if (delete) {
                //logger.info("To delete: " + ind.getIRI().toString().trim());
                ind.accept(remover);
            }
            delete = true;
        }
        manager.applyChanges(remover.getChanges());
        logger.info("Number of individuals after reduction: " + ontology.getIndividualsInSignature().size());
    }

    /**
     * @return the maxPositiveExamples
     */
    public int getMaxPositiveExamples() {
        return maxPositiveExamples;
    }

    /**
     * @param maxPositiveExamples the maxPositiveExamples to set
     */
    public void setMaxPositiveExamples(int maxPositiveExamples) {
        this.maxPositiveExamples = maxPositiveExamples;
    }

    /**
     * @return the maxNegativeExamples
     */
    public int getMaxNegativeExamples() {
        return maxNegativeExamples;
    }

    /**
     * @param maxNegativeExamples the maxNegativeExamples to set
     */
    public void setMaxNegativeExamples(int maxNegativeExamples) {
        this.maxNegativeExamples = maxNegativeExamples;
    }

//    /**
//     * @return the learnParametersFromScratch
//     */
//    public boolean isLearnParametersFromScratch() {
//        return learnParametersFromScratch;
//    }
//
//    /**
//     * @param learnParametersFromScratch the learnParametersFromScratch to set
//     */
//    public void setLearnParametersFromScratch(boolean learnParametersFromScratch) {
//        this.learnParametersFromScratch = learnParametersFromScratch;
//    }
}

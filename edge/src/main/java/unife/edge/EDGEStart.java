/**
 *  This file is part of LEAP.
 * 
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, 
 *  but some components can be used as stand-alone.
 * 
 *  LEAP is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  LEAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package unife.edge;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import unife.bundle.logging.BundleLoggerFactory;

/**
 *
 * @author Riccardo Zese <riccardo.zese@unife.it>, Giuseppe Cota
 * <giuseta@gmail.com>
 */
public class EDGEStart {

    public static EDGEUI getUI(String[] args, boolean mpi) {

        Options options = new Options();
        CommandLine cmd;

//        Option help = new Option("h", "help", false, "print this message");
//        options.addOption(help);
        Option gui = new Option("gui", "run GUI");
        options.addOption(gui);

        CommandLineParser parser = new PosixParser();
        try {
            // parse the command line arguments
            cmd = parser.parse(options, args, true);
            if (cmd.hasOption("gui")) {
                return new EDGEGUI();
            } else {
                return new EDGECLIImpl(mpi);
            }
        } catch (ParseException ex) {
            org.apache.log4j.Logger.getLogger(EDGEStart.class.getName(), new BundleLoggerFactory()).error(ex.getMessage());
            return null;
        }
    }

    public static EDGEUI getUI(String[] args) {
        return getUI(args, false);
    }

}

/**
 *  This file is part of LEAP.
 * 
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, 
 *  but some components can be used as stand-alone.
 * 
 *  LEAP is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  LEAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package unife.rmi.communication;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

/**
 *
 * @author Giuseppe Cota <giuseta@gmail.com>, Riccardo Zese <riccardo.zese@unife.it>
 */
public class RMISerializableInputStream extends InputStream implements
        Serializable {

    RMIInputStream in;

    public RMISerializableInputStream(RMIInputStream in) {
        this.in = in;
    }

    @Override
    public int read() throws IOException {
        return in.read();
    }

    @Override
    public int read(byte[] bytes, int off, int len) throws IOException {
        byte[] readBytes = in.readBytes(len);
        if (readBytes == null) {
            return -1;
        }
        int i = readBytes.length;
        System.arraycopy(readBytes, 0, bytes, off, i);
        return i;
    }

    @Override
    public void close() throws IOException {
        super.close();
    }

}

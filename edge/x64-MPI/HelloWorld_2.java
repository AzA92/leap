/*
import mpi.*;
import java.io.*;
public class HelloWorld_2 {

	public static void main(String args[]) throws Exception {

//MPI.Init(args);
//int me = MPI.COMM_WORLD.Rank();
//int size = MPI.COMM_WORLD.Size();
////System.out.println("Hi from <"+me+">");
//PrintWriter writer = new PrintWriter("/plx/userexternal/rzese000/MPIJava/the-file-"+me+".txt");
//writer.println("The first line");
//writer.println("The second line");
//writer.close();
//MPI.Finalize();

		MPI.Init(args) ;
 		int myrank = MPI.COMM_WORLD.Rank() ;
 		if(myrank == 0) {
 			char[] message = {'h'};
 			PrintWriter writer = new PrintWriter("/plx/userexternal/rzese000/MPIJava/the-file-"+myrank+".txt");
 			MPI.COMM_WORLD.Send(message, 0, message.length, MPI.CHAR, 1, 99) ;
 			
 		} else {
 			char[] message = new char [20] ;
 			message[0] = 'h';
 			//PrintWriter writer = new PrintWriter("/plx/userexternal/rzese000/MPIJava/the-file-"+myrank+".txt");
 			//MPI.COMM_WORLD.Recv(message, 0, 1, MPI.CHAR, 0, 99) ;
 			System.out.println("received:" + new String(message) + ":") ;
 		}
 		MPI.Finalize() ;
 	}
}

*/



import mpi.*;
import java.io.*;
public class HelloWorld_2 {

	public static void main(String args[]) throws Exception {
/*
MPI.Init(args);
int me = MPI.COMM_WORLD.Rank();
int size = MPI.COMM_WORLD.Size();
//System.out.println("Hi from <"+me+">");
PrintWriter writer = new PrintWriter("/plx/userexternal/rzese000/MPIJava/the-file-"+me+".txt");
writer.println("The first line");
writer.println("The second line");
writer.close();
MPI.Finalize();
*/
		
		MPI.Init(args) ;
		
 		int myrank = MPI.COMM_WORLD.getRank() ;
 		if(myrank == 0) {
 			// Uso sempre vettori perchè altrimenti invia e/o  riceve male
 			char[] message= new char[1];
 			
 			//message = "prova               ".toCharArray();
 			
 			System.out.println(message);
 			message[0] = 'c' ;
 			PrintWriter writer = new PrintWriter("/plx/userexternal/rzese000/MPILocal/openmpi-v1.8.3-190-gd64f72b/myTest/res/the-file-"+myrank+".txt");
 			writer.println("send:" + new String(message) + ":");
 			MPI.COMM_WORLD.send(message, 1, MPI.CHAR, 1, 1) ;
 			// Uso sempre vettori perchè altrimenti invia e/o  riceve male
 			int[] i = {5};
 			writer.println("send:" + i[0] + ":");
 			MPI.COMM_WORLD.send(i, 1, MPI.INT, 1, 2) ;
 			
 			Integer inte = new Integer(1);
 			writer.println("send:" + inte + ":");
			sendObject(inte,1,2);
 			
 			Employee e = new Employee();
		        e.name = "Reyan Ali";
		        e.address = "Phokka Kuan, Ambehta Peer";
		        e.SSN = 11122333;
		        e.number = 101;
		        writer.println("send:" + e + ":");
 			sendObject(e,1,2);
 			
 			// Uso sempre vettori perchè altrimenti invia e/o  riceve male
 			int pi[] = {1};
 			double[] pf = {3.0};
 			char[] pc = {'p'};
 			
 			byte[] po = new byte[100];
 			writer.println("packing:" + pi[0] + ": inviato 2 volte");
 			int pos = MPI.COMM_WORLD.pack(pi,1,MPI.INT,po,0);
 			pos = MPI.COMM_WORLD.pack(pi,1,MPI.INT,po,pos);
			writer.println("packing:" + pf[0] + ":");
 			pos = MPI.COMM_WORLD.pack(pf,1,MPI.DOUBLE,po,pos);
 			writer.println("packing:" + pc[0] + ":");
 			pos = MPI.COMM_WORLD.pack(pc,1,MPI.CHAR,po,pos);
 			// Uso sempre vettori perchè altrimenti invia e/o  riceve male
 			i[0]=pos;
 			writer.println("send dim:" + pos + ":");
 			MPI.COMM_WORLD.send(i, 1, MPI.INT, 1, 2) ;
 			writer.println("send pack::");
 			MPI.COMM_WORLD.send(po, pos, MPI.PACKED, 1, 2) ;
 			
 			writer.close();
 			
 		} else {
 			// Uso sempre vettori perchè altrimenti invia e/o  riceve male
 			char[] message = new char [1] ;
 			int[] i = new int[1];
 			
 			PrintWriter writer = new PrintWriter("/plx/userexternal/rzese000/MPILocal/openmpi-v1.8.3-190-gd64f72b/myTest/res/the-file-"+myrank+".txt");
 			
 			MPI.COMM_WORLD.recv(message, 1, MPI.CHAR, 0, 1);
 			System.out.println("received:"  + new String(message) + ":");
 			writer.println("received:" + new String(message) + ":");
 			MPI.COMM_WORLD.recv(i, 1, MPI.INT, 0, 2);
 			System.out.println("received:"  + i[0] +":");
 			writer.println("received:" + i[0] + ":");
 			
 			Integer inte;
 			inte = (Integer)recvObject(0,2);
 			System.out.println("received:"  + inte +":");
 			writer.println("received:" + inte + ":");
 			
 			Employee es = (Employee)recvObject(0,2);
 			System.out.println("received:"  + es +":");
 			writer.println("received:" + es + ":");
 			
 			// Uso sempre vettori perchè altrimenti invia e/o  riceve male
 			i = new int[1];
 			byte[] po = new byte[100];
 			
 			MPI.COMM_WORLD.recv(i, 1, MPI.INT, 0, 2);
 			// Uso sempre vettori perchè altrimenti invia e/o  riceve male
 			int pos=i[0];
 			i = new int[2];
 			double[] di = new double[1];
 			message = new char[1];
 			MPI.COMM_WORLD.recv(po, pos, MPI.PACKED, 0, 2) ;
 			pos = MPI.COMM_WORLD.unpack(po,0,i,2,MPI.INT);
 			pos = MPI.COMM_WORLD.unpack(po,pos,di,1,MPI.DOUBLE);
			pos = MPI.COMM_WORLD.unpack(po,pos,message,1,MPI.CHAR);
 			writer.println("received:" + i[0] + ":");
 			writer.println("received:" + i[1] + ":");
 			writer.println("received:" + di[0] + ":");
 			writer.println("received:" + message[0] + ":");
 			
 			
 			
 			writer.close();
 			
 			
 		}
 		MPI.Finalize() ;
 	}
 	
 	
 	
 	// Translate a generic SERIALIZABLE object into a byte array
	private static byte[] objectToByteArray(Object obj) 
		            throws Exception  {
	    ByteArrayOutputStream bos = new ByteArrayOutputStream();
	    ObjectOutput out = null;
	    byte[] yourBytes = null;
	    try {
		out = new ObjectOutputStream(bos);   
		out.writeObject(obj);
		yourBytes = bos.toByteArray();
	    } finally {
		try {
		    if (out != null) {
		        out.close();
		    }
		} catch (IOException ex) {
		    // ignore close exception
		}
		try {
		    bos.close();
		} catch (IOException ex) {
		    // ignore close exception
		}
	    }
	    return yourBytes;
	}

	// Translate a byte array into a java.lang.Object object
	private static Object byteArrayToObject(byte bytes[])
		            throws Exception  {
	    ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
	    ObjectInput in = null;
	    Object o = null;
	    try {
		in = new ObjectInputStream(bis);
		o = in.readObject(); 
	    } finally {
		try {
		    bis.close();
		} catch (IOException ex) {
		    // ignore close exception
		}
		try {
		    if (in != null) {
		        in.close();
		    }
		} catch (IOException ex) {
		    // ignore close exception
		}
	    }
	    return o;
	}

	// Send a generic object to dest with tag
	private static void sendObject(Object obj, int dest, int tag) throws Exception {
	    // Translate the object into byte array
	    byte[] bytes = objectToByteArray(obj);
	    // Save length of byte array
	    int[] sizePack = {bytes.length};
	    // Send size of byte array
	    MPI.COMM_WORLD.send(sizePack, 1, MPI.INT, dest, tag) ;
	    // Send byte array
	    MPI.COMM_WORLD.send(bytes, sizePack[0], MPI.BYTE, dest, tag) ;
	}

	// Receive a generic object from dest with tag
	private static Object recvObject(int dest, int tag) throws Exception {
	    byte[] bytes;
	    int[] i = new int[1];
	    // Receive the length of the byte array
	    MPI.COMM_WORLD.recv(i, 1, MPI.INT, dest, tag);
	    // Create the correct buffer
	    bytes = new byte[i[0]];
	    // Receive the byte array
	    MPI.COMM_WORLD.recv(bytes, i[0], MPI.BYTE, dest, tag);
	    // Translate the byte array into object
	    return byteArrayToObject(bytes);
	}
 	
 	
}



class Employee implements java.io.Serializable
{
   public String name;
   public String address;
   public transient int SSN;
   public int number;
   public String toString()
   {
      return "Deserialized Employee...\nName: " + name 
      		+"\nAddress: " + address
      		+"\nSSN: " + SSN
      		+"\nNumber: " + number;
   }
}

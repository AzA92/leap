/**
 *  This file is part of LEAP.
 * 
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, 
 *  but some components can be used as stand-alone.
 * 
 *  LEAP is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  LEAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package unife.bundle.renderer;

import java.io.IOException;
import java.util.Set;
import org.apache.log4j.Logger;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLException;

/**
 *
 * @author Riccardo Zese <riccardo.zese@unife.it>, Giuseppe Cota <giuseta@gmail.com>
 */
public class LogManchesterSyntaxExplanationRenderer {

    protected LogManchesterSyntaxObjectRenderer renderer;

    protected Logger logger;

    protected OWLAxiom currentAxiom;

    private int index;

    public LogManchesterSyntaxExplanationRenderer() {
    }

    /**
     * end of rendering.
     *
     * @throws org.semanticweb.owlapi.model.OWLException
     * @throws IOException IO troubles
     */
    public void endRendering() throws OWLException, IOException {
        renderer.flush();
        renderer = null;
    }

    /**
     * Returns the current axioms being whose explanation is being rendered or
     * <code>null</code> if no axiom has been provided.
     *
     * @return the current axioms being whose explanation is being rendered or
     * <code>null</code> if no axiom has been provided
     */
    protected OWLAxiom getCurrentAxiom() {
        return currentAxiom;
    }

    /**
     * Render an explanation without the axiom header. This function is not
     * guaranteed to be supported by the subclasses since an explanation
     * renderer may rely on the axiom being explained to reorder the axioms or
     * find irrelevant bits.
     *
     * @param explanations Set of explanations we are rendering
     * @throws OWLException
     * @throws IOException
     * @throws UnsupportedOperationException
     */
    public void render(Set<Set<OWLAxiom>> explanations) throws OWLException,
            IOException, UnsupportedOperationException {
        render((OWLAxiom) null, explanations);
    }

    /**
     * Render.
     *
     * @param axiom the axiom to render
     * @param explanations the explanations to render
     * @throws OWLException OWL troubles
     * @throws IOException IO troubles
     */
    public void render(OWLAxiom axiom, Set<Set<OWLAxiom>> explanations) throws OWLException,
            IOException {
        setCurrentAxiom(axiom);

        if (index == 1) {
            if (axiom != null) {
                renderer.write("Axiom: ");
                axiom.accept(renderer);
                renderer.writeNewLine();
                renderer.writeNewLine();
            }
            if (explanations.isEmpty()) {
                renderer.writeln("Explanation: AXIOM IS NOT ENTAILED!");
                return;
            }
            renderer.writeln("Explanation(s): ");
        }

        String header = index++ + ")";
        renderer.write(header);
        renderSingleExplanation(explanations.iterator().next());
        //renderer.flush();
    }

    protected void renderSingleExplanation(Set<OWLAxiom> explanation) throws OWLException,
            IOException {

        renderer.startBlock(3);

        for (OWLAxiom a : explanation) {
            a.accept(renderer);
            renderer.writeNewLine();
        }

        renderer.endBlock();
        renderer.writeNewLine();
    }

    protected void setCurrentAxiom(OWLAxiom currentAxiom) {
        this.currentAxiom = currentAxiom;
    }

    /**
     * Start rendering.
     *
     * @param logger the logger to use
     * @throws OWLException OWL troubles
     * @throws IOException IO troubles
     */
    public void startRendering(Logger logger) throws OWLException, IOException {
        this.logger = logger;
        renderer = new LogManchesterSyntaxObjectRenderer(logger);
        index = 1;
    }
    
    public void write(String s) {
        renderer.write(s);
    }
    
    public void writeln(String s) {
        renderer.write(s);
    }
}

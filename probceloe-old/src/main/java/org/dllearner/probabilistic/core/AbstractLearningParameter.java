/**
 *  This file is part of LEAP.
 * 
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, 
 *  but some components can be used as stand-alone.
 * 
 *  LEAP is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  LEAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.dllearner.probabilistic.core;

import unife.edgeRemote.exceptions.InconsistencyException;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import org.dllearner.core.AbstractLearningProblem;
import org.dllearner.core.KnowledgeSource;
import org.dllearner.core.LearningAlgorithm;
import org.dllearner.core.LearningProblem;
import org.dllearner.core.config.ConfigOption;
import org.dllearner.core.owl.Axiom;
import org.dllearner.core.owl.Description;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Abstract component representing a parameter Learner. A parameter learner
 * computes the probabilistic value of a single axiom
 *
 * @author Giuseppe Cota
 */
public abstract class AbstractLearningParameter implements LearningAlgorithm {

    /**
     * set of all the theories
     */
    protected Set<KnowledgeSource> sources;
    /**
     * File URI (String) containing the target axioms, whose parameters we want
     * to learn
     */
    //protected String targetAxioms;
    protected Set<Axiom> targetAxioms;
    protected AbstractLearningProblem learningProblem;

    @ConfigOption(name = "reduceOntology", description = "Force the reduction of the number of individuals in the initial ontology", defaultValue = "false")
    protected boolean reduceOntology = false;

    public AbstractLearningParameter() {

    }

    /**
     *
     * @param learningProblem
     * @param targetAxioms target axioms in manchester syntax
     */
//    public AbstractLearningParameter(AbstractLearningProblem learningProblem, String targetAxioms){
//        this.sources=learningProblem.getReasoner().getSources();
//        this.targetAxioms=targetAxioms;
//        this.learningProblem=learningProblem;
//    }
    public AbstractLearningParameter(AbstractLearningProblem learningProblem, Set<Axiom> targetAxioms) {
        this.sources = learningProblem.getReasoner().getSources();
        this.targetAxioms = targetAxioms;
        this.learningProblem = learningProblem;
    }

    /**
     * DO NOTHING.
     */
    @Override
    public void start() {

    }

    public abstract void run();

    /**
     * Sets the target axioms of which want to learn the parameters.
     *
     * @param targetAxioms
     */
    public void setTargetAxioms(Set<Axiom> targetAxioms) {
        this.targetAxioms = targetAxioms;
    }

    /**
     * Gets the target axioms.
     *
     * @return target axioms
     */
    public Set<Axiom> getTargetAxioms() {
        return targetAxioms;
    }

    /**
     * Gets the probabilistic parameter of an axiom
     *
     * @param ax
     * @return the probabilistic parameter of the axiom
     * @throws org.dllearner.probabilistic.core.ParameterLearnerException
     */
    public abstract double getParameter(Axiom ax) throws ParameterLearnerException;

    /**
     * Get the Log-Likelihood of all the examples/queries.
     *
     * @return the log-likelihood of all the examples/queries
     */
    public abstract double getLL();

    /**
     * Gets the map of the time in milliseconds spent by various algorithms.
     *
     * @return
     */
    public abstract Map<String, Long> getTimeMap();

    // TO DO aggiungere getLL che restituisce la log likelihood di un singolo esempio
    /**
     * Gets the knowledge sources used by this parameters learner
     *
     * @return the underlying knowledge sources
     */
    public Set<KnowledgeSource> getSources() {
        return sources;
    }

    /**
     * Method to change the knowledge sources underlying the parameter learner.
     * Changes only the knowledge sources contained in this class. Does not
     * affect other classes
     *
     * @param sources The new knowledge sources.
     */
    public void changeSources(Set<KnowledgeSource> sources) {
        this.sources = sources;
    }

    /**
     * Gets the target axioms whose parameters we want to learn
     *
     * @return the underlying target axioms
     */
//    public String getTargetAxioms() {
//        return targetAxioms;
//    }
    /**
     * Changes the target axioms underlying the parameter learner
     *
     * @param the underlying target axioms
     */
//    public void changeTargetAxioms(String targetAxioms) {
//        this.targetAxioms = targetAxioms;
//    }
    /**
     * The learning problem variable, which must be used by all learning
     * algorithm implementations.
     */
    @Override
    public AbstractLearningProblem getLearningProblem() {
        return learningProblem;
    }

    @Override
    @Autowired
    public void setLearningProblem(LearningProblem learningProblem) {
        this.learningProblem = (AbstractLearningProblem) learningProblem;
    }

    /**
     * Adds the axiom classExpression subClassOf learnedClass into the ontology.
     * learnedClass is a dummy class that will be substituted if our learning
     * problem is a Class Expression Learning.
     *
     * @param classExpression
     * @param epistemicProb
     * @throws ParameterLearnerException
     * @throws unife.edgeRemote.exceptions.InconsistencyException
     */
    public abstract void addAxiom(Description classExpression, double epistemicProb) throws ParameterLearnerException, InconsistencyException;

    /**
     * Saves the learned ontology containing only the learned probabilistic
     * subClassOf axioms.
     *
     * @throws ParameterLearnerException
     * @throws java.io.IOException
     */
    public abstract void saveLearnedOntology(File learnedOntologyFile) throws ParameterLearnerException, IOException;

    /**
     * Gets the complete learned ontology containing both non-probabilistic
     * axioms and probabilistic axioms
     *
     * @param completeLearnedOntologyFile
     * @throws ParameterLearnerException
     * @throws java.io.IOException
     */
    public abstract void saveCompleteLearnedOntology(File completeLearnedOntologyFile) throws ParameterLearnerException, IOException;

    /**
     * Updates the current learned ontology. This is a mandatory method to call
     * when the Log-Likelihood improved. This is a method that you must call if
     * you want to update the probability values of the learned ontology, after
     * it was proved that the Log-Likelihood improved. Do not call it after a
     * {@link #removeLastAxiom()} or you will get wrong values.
     *
     * @throws ParameterLearnerException
     */
    public abstract void updateOntology() throws ParameterLearnerException;

    /**
     * Removes the last added axiom.
     *
     * @throws ParameterLearnerException
     */
    public abstract void removeLastAxiom() throws ParameterLearnerException;

    /**
     * Saves the ontology containing the positive examples.
     *
     * @throws ParameterLearnerException
     * @throws IOException
     */
    public abstract void savePositiveExamples() throws ParameterLearnerException, IOException;

    /**
     * Saves the ontology containing the negative examples.
     *
     * @throws ParameterLearnerException
     * @throws IOException
     */
    public abstract void saveNegativeExamples() throws ParameterLearnerException, IOException;

    /**
     * Stops the algorithm.
     *
     */
    public abstract void stop();

    /**
     * @return the reduceOntology
     */
    public boolean isReduceOntology() {
        return reduceOntology;
    }

    /**
     * @param reduceOntology the reduceOntology to set
     */
    public void setReduceOntology(boolean reduceOntology) {
        this.reduceOntology = reduceOntology;
    }
}

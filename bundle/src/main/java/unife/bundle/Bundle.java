/**
 * This file is part of LEAP.
 *
 * LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, but
 * some components can be used as stand-alone.
 *
 * LEAP is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * LEAP is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package unife.bundle;

import com.clarkparsia.owlapi.explanation.BlackBoxExplanation;
import com.clarkparsia.owlapi.explanation.GlassBoxExplanation;
import com.clarkparsia.owlapi.explanation.MultipleExplanationGenerator;
import com.clarkparsia.owlapi.explanation.SatisfiabilityConverter;
import com.clarkparsia.owlapi.explanation.TransactionAwareSingleExpGen;
import com.clarkparsia.owlapiv3.OWL;
import com.clarkparsia.owlapiv3.OntologyUtils;
import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import net.sf.javabdd.BDD;
import net.sf.javabdd.BDDFactory;
import net.sf.javabdd.BuDDyFactory;
import org.apache.log4j.Logger;
import org.semanticweb.owlapi.util.mansyntax.ManchesterOWLSyntaxParser;
import org.mindswap.pellet.PelletOptions;
import org.mindswap.pellet.utils.Timer;
import org.mindswap.pellet.utils.progress.ConsoleProgressMonitor;
import org.mindswap.pellet.utils.progress.ProgressMonitor;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.manchestersyntax.renderer.ParserException;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLException;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObject;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLProperty;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;
import org.semanticweb.owlapi.reasoner.Node;
import org.semanticweb.owlapi.reasoner.NodeSet;
import pellet.PelletCmdApp;
import pellet.PelletCmdException;
import pellet.PelletCmdOption;
import static pellet.PelletCmdOptionArg.NONE;
import static pellet.PelletCmdOptionArg.REQUIRED;
import pellet.PelletCmdOptions;
import unife.bundle.explanation.BundleGlassBoxExplanation;
import unife.bundle.explanation.BundleHSTExplanationGenerator;
import unife.bundle.logging.BundleLoggerFactory;
import unife.bundle.monitor.BundleRendererExplanationProgressMonitor;
import unife.bundle.monitor.LogRendererTimeExplanationProgressMonitor;
import unife.bundle.monitor.ScreenRendererTimeExplanationProgressMonitor;
import unife.bundle.utilities.BundleUtilities;
import unife.math.utilities.MathUtilities;

/**
 *
 * Bundle's core class. Extends PelletCmdApp class from the Pellet code and
 * allows the computation of the given queries.
 *
 * @author Riccardo Zese <riccardo.zese@unife.it>, Giuseppe Cota
 * <giuseta@gmail.com>
 */
public class Bundle extends PelletCmdApp {

    private SatisfiabilityConverter converter;
    private final Logger logger = Logger.getLogger(Bundle.class.getName(), new BundleLoggerFactory());
    /**
     * inferences for which there was an error while generating the explanation
     */
    private int errorExpCount = 0;

    private int maxExplanations = Integer.MAX_VALUE;
    private int maxTime = 0;
    private boolean useBlackBox = false;
    private ProgressMonitor monitor;

    /**
     * inferences whose explanation contains more than on axiom
     */
    private int multiAxiomExpCount = 0;

    /**
     * inferences with multiple explanations
     */
    private int multipleExpCount = 0;
    private PelletReasoner reasoner;
    private OWLEntity name1;
    private OWLEntity name2;
    private OWLObject name3;
    protected boolean findProbability = true;
    private boolean log = false;

    /**
     * background ontology
     */
    private OWLOntology baseOntology;

    /**
     * ontology containing the probabilistic target axioms
     */
    private String probOntologyName = null;

    private Map<OWLAxiom, BigDecimal> PMap = null;

    private BigDecimal[] prob = new BigDecimal[0];
    private String[] axp = new String[0];
    private OWLAxiom[] axi = new OWLAxiom[0];

    private String bddFType = "buddy";
    private BDDFactory bddF;

    private int accuracy = 5;

    /**
     * Sets up the class and the explanation generator.
     */
    public Bundle() {
        GlassBoxExplanation.setup();
    }

    @Override
    public String getAppId() {
        return "Bundle: Explains one or more inferences in a given ontology including ontology inconsistency";
    }

    @Override
    public String getAppCmd() {
        return "bundle " + getMandatoryOptions() + "[options] <file URI>...\n\n"
                + "The options --unsat, --all-unsat, --inconsistent, --subclass, \n"
                + "--hierarchy, and --instance are mutually exclusive. By default \n "
                + "--inconsistent option is assumed. In the following descriptions \n"
                + "C, D, and i can be URIs or local names.";
    }

    /**
     * Sets up and returns the set of the arguments Bundle accepts.
     *
     * @return a PelletCmdOption object containing all the argument accepted by
     * Bundle
     */
    @Override
    public PelletCmdOptions getOptions() {
        PelletCmdOptions options = getGlobalOptions();

        options.add(getIgnoreImportsOption());

        PelletCmdOption option = new PelletCmdOption("unsat");
        option.setType("C");
        option.setDescription("Explain why the given class is unsatisfiable");
        option.setIsMandatory(false);
        option.setArg(REQUIRED);
        options.add(option);

        option = new PelletCmdOption("all-unsat");
        option.setDescription("Explain all unsatisfiable classes");
        option.setDefaultValue(false);
        option.setIsMandatory(false);
        option.setArg(NONE);
        options.add(option);

        option = new PelletCmdOption("inconsistent");
        option.setDescription("Explain why the ontology is inconsistent");
        option.setDefaultValue(false);
        option.setIsMandatory(false);
        option.setArg(NONE);
        options.add(option);

        option = new PelletCmdOption("hierarchy");
        option.setDescription("Print all explanations for the class hierarchy");
        option.setDefaultValue(false);
        option.setIsMandatory(false);
        option.setArg(NONE);
        options.add(option);

        option = new PelletCmdOption("subclass");
        option.setDescription("Explain why C is a subclass of D");
        option.setType("C,D");
        option.setIsMandatory(false);
        option.setArg(REQUIRED);
        options.add(option);

        option = new PelletCmdOption("instance");
        option.setDescription("Explain why i is an instance of C");
        option.setType("i,C");
        option.setIsMandatory(false);
        option.setArg(REQUIRED);
        options.add(option);

        option = new PelletCmdOption("property-value");
        option.setDescription("Explain why s has value o for property p");
        option.setType("s,p,o");
        option.setIsMandatory(false);
        option.setArg(REQUIRED);
        options.add(option);

        option = new PelletCmdOption("method");
        option.setShortOption("m");
        option.setType("glass | black");
        option.setDescription("Method that will be used to generate explanations");
        option.setDefaultValue("glass");
        option.setIsMandatory(false);
        option.setArg(REQUIRED);
        options.add(option);

        option = new PelletCmdOption("max");
        option.setShortOption("x");
        option.setType("positive integer");
        option.setDescription("Maximum number of generated explanations for each inference");
        option.setDefaultValue(Integer.MAX_VALUE);
        option.setIsMandatory(false);
        option.setArg(REQUIRED);
        options.add(option);

        option = new PelletCmdOption("time");
        option.setShortOption("t");
        option.setDescription("Maximum time allowed for the inference, 0 for unlimited time. Format: [Xh][Ym][Zs][Kms]");
        option.setDefaultValue("0s");
        option.setIsMandatory(false);
        option.setArg(REQUIRED);
        options.add(option);

        option = new PelletCmdOption("noProb");
        option.setDescription("Disable the computation of the probability");
        option.setDefaultValue(false);
        option.setIsMandatory(false);
        option.setArg(NONE);
        options.add(option);

        option = new PelletCmdOption("log");
        option.setDescription("Write on log instead of on screen");
        option.setDefaultValue(false);
        option.setIsMandatory(false);
        option.setArg(NONE);
        options.add(option);
        
        option = new PelletCmdOption("bddfact");
        option.setShortOption("bf");
        option.setDescription("Set the BDD Factory, possible values can be "
                + "\"buddy\", \"cudd\", \"j\", \"java\", \"jdd\", \"test\", "
                + "\"typed\", or a name of a class that has an init() method "
                + "that returns a BDDFactory. Note: \"cal\" is not supported, "
                + "use \"buddy\" for better performances. If the loading fails "
                + "I will use \"java\" factory.");
        option.setDefaultValue("buddy");
        option.setIsMandatory(false);
        option.setArg(REQUIRED);
        options.add(option);

        option = options.getOption("verbose");
        option.setDescription("Print detailed exceptions and messages about the progress");

        return options;
    }

    /**
     * Parses the array containing the arguments and configures Bundle.
     *
     * @param args the array of arguments
     */
    @Override
    public void parseArgs(String[] args) {

        // Shift of the arguments because PelletCmdApp reads args array from the position 1.
        String[] argsNew = new String[args.length + 1];
        argsNew[0] = "BUNDLE";
        int argsNewIdx = 1;
        for (String arg : args) {
            if (!arg.equals("-v")) {
                argsNew[argsNewIdx] = arg;
                argsNewIdx++;
            } else {
                verbose = true;
            }
        }
        //System.arraycopy(args, 0, argsNew, 1, args.length);
        //System.arraycopy(new String[]{"null", "null"}, 0, argsNew, argsNew.length, 2);
        args = argsNew;
        super.parseArgs(args);

        String bddFactoryName = options.getOption("bddfact").getValueAsString();
        if (bddFactoryName.equals("cal")) {
            if (log) 
                logger.info("CAL is not supported as BDD Factory. I will use BuDDy!");
            System.out.println("CAL is not supported as BDD Factory. I will use BuDDy!");
        } else {
            setBddFType(bddFactoryName);
        }
        setMaxExplanations(options.getOption("max").getValueAsNonNegativeInteger());
        setMaxTime(BundleUtilities.convertTimeValue(options.getOption("time").getValueAsString()));
        findProbability = !options.getOption("noProb").getValueAsBoolean();
        setLog(options.getOption("log").getValueAsBoolean());
        if (verbose) {
            if (getMaxExplanations() != Integer.MAX_VALUE) {
                if (log) {
                    logger.info("Max Explanations: " + getMaxExplanations());
                } else {
                    System.out.println("Max Explanations: " + getMaxExplanations());
                }
            }
            if (getMaxTime() != 0) {
                if (log) {
                    logger.info("Max Execution Time: " + options.getOption("time").getValue() + " (" + getMaxTime() + ")");
                }
                System.out.println("Max Execution Time: " + options.getOption("time").getValue() + " (" + getMaxTime() + ")");
            }
            if (findProbability != true) {
                if (log) {
                    logger.info("No probability computation");
                }
                System.out.println("No probability computation");
            }
        }

        String inputFiles = getInputFiles()[0];
        loadOntologies(inputFiles);
        if (getInputFiles().length > 1) {
            probOntologyName = getInputFiles()[1];
        }

        //loader = (OWLAPILoader) getLoader("OWLAPIv3");
        //getKB(loader, 0);
        if (findProbability) {
            verbose("\n\nLoading probabilities...");
            setPMap();
            if (PMap == null || PMap.isEmpty())
                findProbability = false;
        }

        loadMethod();
        loadNames();

    }

    /**
     * Loads the ontology given as argument.
     */
    public void loadOntologies() {
        loadOntologies("");
    }

    /**
     * Loads the ontology given as argument or as parameter of the method. If
     * the argument is null, the method loads the ontology given by the
     * arguments, otherwise loads the given ontology
     *
     * @param inputFiles null or the IRI of the ontology file
     */
    public void loadOntologies(String inputFiles) {

        if (inputFiles == null || inputFiles.isEmpty()) {
            inputFiles = getInputFiles()[0];
        }

        try {
            OWLOntology ontology = OWLManager.createOWLOntologyManager().loadOntologyFromOntologyDocument(IRI.create(inputFiles));
            loadOntologies(ontology);
        } catch (OWLOntologyCreationException e) {
            throw new RuntimeException(e.getMessage());
        }

        //converter = new SatisfiabilityConverter(loader.getManager().getOWLDataFactory());
        //reasoner = loader.getReasoner();
        //converter = new SatisfiabilityConverter(baseOntology.getOWLOntologyManager().getOWLDataFactory());
        //reasoner = new PelletReasonerFactory().createReasoner(baseOntology);
    }

    public void loadOntologies(OWLOntology baseOntology) {
        this.baseOntology = baseOntology;
        converter = new SatisfiabilityConverter(this.baseOntology.getOWLOntologyManager().getOWLDataFactory());
        reasoner = new PelletReasonerFactory().createReasoner(this.baseOntology);
    }

//    protected KnowledgeBase getKB(KBLoader loader, int file) {
//        try {
//            String[] inputFiles = getInputFiles();
//
//            verbose("There are " + 1 + " input files:");
//            verbose(inputFiles[file]);
//            
//
//            startTask("loading");
//            KnowledgeBase kb = loader.createKB(inputFiles[file]);
//            finishTask("loading");
//
//            if (verbose) {
//                StringBuilder sb = new StringBuilder();
//                sb.append("Classes = " + kb.getAllClasses().size() + ", ");
//                sb.append("Properties = " + kb.getProperties().size() + ", ");
//                sb.append("Individuals = " + kb.getIndividuals().size());
//                verbose("Input size: " + sb);
//
//                verbose("Expressivity: " + kb.getExpressivity());
//            }
//
//            return kb;
//        } catch (RuntimeException e) {
//            throw new PelletCmdException(e);
//        }
//    }
    /**
     * Loads the method used for computing explanations.
     */
    private void loadMethod() {
        String method = options.getOption("method").getValueAsString();

        if (method.equalsIgnoreCase("black")) {
            useBlackBox = true;
        } else if (method.equalsIgnoreCase("glass")) {
            useBlackBox = false;
        } else {
            throw new PelletCmdException("Unrecognized method: " + method);
        }
    }

    /**
     * Processes the arguments specifying the query
     */
    private void loadNames() {
        PelletCmdOption option;

        name1 = name2 = null;
        name3 = null;

        if ((option = options.getOption("hierarchy")) != null) {
            if (option.getValueAsBoolean()) {
                return;
            }
        }

        if ((option = options.getOption("all-unsat")) != null) {
            if (option.getValueAsBoolean()) {
                name1 = OWL.Nothing;
                return;
            }
        }

        if ((option = options.getOption("inconsistent")) != null) {
            if (option.getValueAsBoolean()) {
                if (useBlackBox) {
                    throw new PelletCmdException("Black box method cannot be used to explain ontology inconsistency");
                }
                name1 = OWL.Thing;
                return;
            }
        }

        if ((option = options.getOption("unsat")) != null) {
            String unsatisfiable = option.getValueAsString();
            if (unsatisfiable != null) {
                name1 = OntologyUtils.findEntity(unsatisfiable, baseOntology);

                if (name1 == null) {
                    throw new PelletCmdException("Undefined entity: " + unsatisfiable);
                } else if (!name1.isOWLClass()) {
                    throw new PelletCmdException("Not a defined class: " + unsatisfiable);
                } else if (name1.isTopEntity() && useBlackBox) {
                    throw new PelletCmdException("Black box method cannot be used to explain unsatisfiability of owl:Thing");
                }

                return;
            }
        }

        if ((option = options.getOption("subclass")) != null) {
            String subclass = option.getValueAsString();
            if (subclass != null) {
                String[] names = subclass.split(",");
                if (names.length != 2) {
                    throw new PelletCmdException(
                            "Invalid format for subclass option: " + subclass);
                }

                name1 = OntologyUtils.findEntity(names[0], baseOntology);
                name2 = OntologyUtils.findEntity(names[1], baseOntology);

                if (name1 == null) {
                    throw new PelletCmdException("Undefined entity: " + names[0]);
                } else if (!name1.isOWLClass()) {
                    throw new PelletCmdException("Not a defined class: " + names[0]);
                }
                if (name2 == null) {
                    throw new PelletCmdException("Undefined entity: " + names[1]);
                } else if (!name2.isOWLClass()) {
                    throw new PelletCmdException("Not a defined class: " + names[1]);
                }
                return;
            }
        }

        if ((option = options.getOption("instance")) != null) {
            String instance = option.getValueAsString();
            if (instance != null) {
                String[] names = instance.split(",");
                if (names.length != 2) {
                    throw new PelletCmdException("Invalid format for instance option: " + instance);
                }

                name1 = OntologyUtils.findEntity(names[0], baseOntology);
                name2 = OntologyUtils.findEntity(names[1], baseOntology);

                if (name1 == null) {
                    throw new PelletCmdException("Undefined entity: " + names[0]);
                } else if (!name1.isOWLNamedIndividual()) {
                    throw new PelletCmdException("Not a defined individual: " + names[0]);
                }
                if (name2 == null) {
                    throw new PelletCmdException("Undefined entity: " + names[1]);
                } else if (!name2.isOWLClass()) {
                    throw new PelletCmdException("Not a defined class: " + names[1]);
                }

                return;
            }
        }

        if ((option = options.getOption("property-value")) != null) {
            String optionValue = option.getValueAsString();
            if (optionValue != null) {
                String[] names = optionValue.split(",");
                if (names.length != 3) {
                    throw new PelletCmdException("Invalid format for property-value option: " + optionValue);
                }

                name1 = OntologyUtils.findEntity(names[0], baseOntology);
                name2 = OntologyUtils.findEntity(names[1], baseOntology);

                if (name1 == null) {
                    throw new PelletCmdException("Undefined entity: " + names[0]);
                } else if (!name1.isOWLNamedIndividual()) {
                    throw new PelletCmdException("Not an individual: " + names[0]);
                }
                if (name2 == null) {
                    throw new PelletCmdException("Undefined entity: " + names[1]);
                } else if (!name2.isOWLObjectProperty() && !name2.isOWLDataProperty()) {
                    throw new PelletCmdException("Not a defined property: " + names[1]);
                }
                if (name2.isOWLObjectProperty()) {
                    name3 = OntologyUtils.findEntity(names[2], baseOntology);
                    if (name3 == null) {
                        throw new PelletCmdException("Undefined entity: " + names[2]);
                    } else if (!(name3 instanceof OWLIndividual)) {
                        throw new PelletCmdException("Not a defined individual: " + names[2]);
                    }
                } else {
                    ManchesterOWLSyntaxParser parser = OWLManager.createManchesterParser();
//                    ManchesterOWLSyntaxEditorParser parser = new ManchesterOWLSyntaxEditorParser(
//                            baseOntology.getOWLOntologyManager().getOWLDataFactory(), names[2]);
                    try {
                        name3 = parser.parseLiteral(null);
                    } catch (ParserException e) {
                        throw new PelletCmdException("Not a valid literal: " + names[2]);
                    }
                }

                return;
            }
        }

        // Per default we explain why the ontology is inconsistent
        name1 = OWL.Thing;
        if (useBlackBox) {
            throw new PelletCmdException("Black box method cannot be used to explain ontology inconsistency");
        }

    }

    /**
     * Instantiates and returns the explanation generator.
     *
     * @return the explanation generator
     */
    private TransactionAwareSingleExpGen getSingleExplanationGenerator() {
        if (useBlackBox) {
            if (options.getOption("inconsistent") != null) {
                if (!options.getOption("inconsistent").getValueAsBoolean()) {
                    return new BlackBoxExplanation(reasoner.getRootOntology(), PelletReasonerFactory.getInstance(), reasoner);
                } else {
                    output("WARNING: black method cannot be used to explain inconsistency. Switching to glass.");
                    //return new BundleGlassBoxExplanation(reasoner);
                    return new BundleGlassBoxExplanation(reasoner);
                }
            } else {
                return new BlackBoxExplanation(reasoner.getRootOntology(), PelletReasonerFactory.getInstance(), reasoner);
            }
        } else {
            return new BundleGlassBoxExplanation(reasoner);
        }
    }

    /**
     * Prints useful statistics if in verbose mode.
     */
    private void printStatistics() {

        if (!verbose) {
            return;
        }

        Timer timer = timers.getTimer("explain");
        if (timer != null) {
            // TODO change with logger????
            verbose("");
            verbose("========== Statistics ==========");
            //verbose("Subclass relations   : " + timer.getCount(), log);
            verbose("Multiple explanations: " + multipleExpCount, log);
            verbose("Single explanation     ", log);
            verbose(" with multiple axioms: " + multiAxiomExpCount, log);
            verbose("Error explaining     : " + errorExpCount, log);
            verbose("Average time         : " + timer.getAverage() + "ms", log);

//            if (verbose) {
//                StringWriter sw = new StringWriter();
//                timers.print(sw, true, null);
//
//                verbose("", log);
//                verbose("Timer summary:", log);
//                String[] lines = sw.toString().split("\n");
//                for (String line : lines) {
//                    verbose(line, log);
//                }
//            }
        }
    }

    /**
     * Disposes Bundle maintaining the content of the PMap.
     */
    @Override
    public void finish() {
        finish(false);
    }

    /**
     * Disposes Bundle.
     *
     * @param clearPMap true for cleaning the PMap, false otherwise
     */
    public void finish(boolean clearPMap) {
        super.finish();
        monitor = null;
        loader = null;
        reasoner.dispose();
        reasoner = null;
        if (clearPMap) {
            PMap.clear();
        }
        PMap = null;
        prob = null;
        axp = null;
        axi = null;
        PelletOptions.USE_TRACING = false;
    }

    /**
     * Resets Bundle.
     */
    public void reset() {
        prob = new BigDecimal[0];
        axp = new String[0];
        axi = new OWLAxiom[0];
        if (reasoner != null) {
            reasoner.dispose();
            reasoner = null;
        }
        if (converter != null) {
            converter = null;
        }
        loadOntologies();
    }

    /**
     * Computes the query given by the arguments.
     *
     * @return the result of the computation contained in a QueryResult object
     */
    public QueryResult computeQuery() {

        QueryResult result = null;
        //setPMap();

        try {
            if (name1 == null) {
                // Option --hierarchy
                verbose("Explain all the subclass relations in the ontology");
                result = explainClassHierarchy();
            } else if (name2 == null) {
                if (((OWLClassExpression) name1).isOWLNothing()) {
                    //xxx
                    verbose("Explain all the unsatisfiable classes");
                    result = explainUnsatisfiableClasses();
                } else {
                    // Option --inconsistent && --unsat C
                    verbose("Explain unsatisfiability of " + name1);
                    result = explainUnsatisfiableClass((OWLClass) name1);
                }
            } else if (name3 != null) {
                // Option --property-value s,p,o
                verbose("Explain property assertion " + name1 + " and " + name2 + " and " + name3);

                result = explainPropertyValue((OWLIndividual) name1, (OWLProperty) name2, name3);
            } else if (name1.isOWLClass() && name2.isOWLClass()) {
                // Option --subclass C,D
                verbose("Explain subclass relation between " + name1 + " and " + name2);

                result = explainSubClass((OWLClass) name1, (OWLClass) name2);
            } else if (name1.isOWLNamedIndividual() && name2.isOWLClass()) {
                // Option --instance i,C
                verbose("Explain instance relation between " + name1 + " and " + name2);

                result = explainInstance((OWLIndividual) name1, (OWLClass) name2);
            }
            //printStatistics();

        } catch (OWLException e) {
            if (findProbability && getBddF() != null) {
                getBddF().done();
            }
            throw new RuntimeException(e);
        }
        if (result == null) {
            throw new RuntimeException("Explanations not found");
        }

        computeProbability(result);
        printResult(result);
        if (verbose) {
            printStatistics();
        }
        return result;
    }

    /**
     * Computes the given query.
     *
     * @param query the query to compute
     * @return the result of the computation contained in a QueryResult object
     * @throws org.semanticweb.owlapi.model.OWLException
     */
    public QueryResult computeQuery(OWLAxiom query) throws OWLException {
        QueryResult result = explainAxiom(query);
        computeProbability(result);
        return result;
    }
    
    public QueryResult computeAllUnsatQuery() {
        name1 = OWL.Nothing;
        QueryResult result = computeQuery();
        return result;
    }
    
    public QueryResult computeHierarchyQuery() {
        QueryResult result = computeQuery();
        return result;
    }

    /**
     * Executes a class hierarchy query.
     *
     * @return the result of the computation contained in a QueryResult object
     * @throws OWLException
     */
    private QueryResult explainClassHierarchy() throws OWLException {
        Set<OWLClass> visited = new HashSet<>();

        reasoner.flush();

        startTask("Classification");
        reasoner.getKB().classify();
        finishTask("Classification");

        startTask("Realization");
        reasoner.getKB().realize();
        finishTask("Realization");

        monitor = new ConsoleProgressMonitor();
        monitor.setProgressTitle("Explaining");
        monitor.setProgressLength(reasoner.getRootOntology().getClassesInSignature().size());
        monitor.taskStarted();

        Node<OWLClass> bottoms = reasoner.getEquivalentClasses(OWL.Nothing);
        QueryResult topsResults = explainClassHierarchy(OWL.Nothing, bottoms, visited);

        Node<OWLClass> tops = reasoner.getEquivalentClasses(OWL.Thing);
        QueryResult bottomsResults = explainClassHierarchy(OWL.Thing, tops, visited);

        monitor.taskFinished();

        topsResults.merge(bottomsResults);
        bottomsResults = null; //cleaning
        return topsResults;

    }

    /**
     * Computes the explanations for the hierarchy (hierarchy). It starts from
     * cls and visits all the classes contained in eqClasses
     *
     * @param cls starting class
     * @param eqClasses Node containing the classes to check
     * @param visited the set of visited classes
     * @return the result of the computation contained in a QueryResult object
     * @throws OWLException
     */
    private QueryResult explainClassHierarchy(OWLClass cls, Node<OWLClass> eqClasses, Set<OWLClass> visited)
            throws OWLException {

        QueryResult results = new QueryResult();

        if (visited.contains(cls)) {
            return new QueryResult();
        }

        visited.add(cls);
        visited.addAll(eqClasses.getEntities());

        for (OWLClass eqClass : eqClasses) {
            monitor.incrementProgress();

            results.merge(explainEquivalentClass(cls, eqClass));
        }

        for (OWLNamedIndividual ind : reasoner.getInstances(cls, true).getFlattened()) {
            results.merge(explainInstance(ind, cls));
        }

        NodeSet<OWLClass> subClasses = reasoner.getSubClasses(cls, true);
        Map<OWLClass, Node<OWLClass>> subClassEqs = new HashMap<>();
        for (Node<OWLClass> equivalenceSet : subClasses) {
            if (equivalenceSet.isBottomNode()) {
                continue;
            }

            OWLClass subClass = equivalenceSet.getRepresentativeElement();
            subClassEqs.put(subClass, equivalenceSet);
            results.merge(explainSubClass(subClass, cls));
        }

        for (Map.Entry<OWLClass, Node<OWLClass>> entry : subClassEqs.entrySet()) {
            results.merge(explainClassHierarchy(entry.getKey(), entry.getValue(), visited));
        }

        return results;
    }

    /**
     * Searches for unsatisfiable classes (all-unsat). Executes a unsatisfiable
     * class query for every class of the ontology
     *
     * @return the result of the computation contained in a QueryResult object
     * @throws OWLException
     */
    private QueryResult explainUnsatisfiableClasses() throws OWLException {

        QueryResult results = new QueryResult();

        for (OWLClass cls : reasoner.getEquivalentClasses(OWL.Nothing)) {
            if (cls.isOWLNothing()) {
                continue;
            }

            results.merge(explainUnsatisfiableClass(cls));
        }

        return results;
    }

    /**
     * Computes an unsatisfiable class query (unsat).
     *
     * @param owlClass the class to prove the unsatisfiability
     * @return the result of the computation contained in a QueryResult object
     * @throws OWLException
     */
    private QueryResult explainUnsatisfiableClass(OWLClass owlClass) throws OWLException {
        return explainSubClass(owlClass, OWL.Nothing);
    }

    /**
     * Computes a property value query (property-value).
     *
     * @param s the subject of a property assertion
     * @param p the property of the assertion
     * @param o the object of the property assertion
     * @return the result of the computation contained in a QueryResult object
     * @throws OWLException
     */
    // In the following method(s) we intentionally do not use OWLPropertyExpression<?,?>
    // because of a bug in some Sun's implementation of javac
    // http://bugs.sun.com/view_bug.do?bug_id=6548436
    // Since lack of generic type generates a warning, we suppress it
    @SuppressWarnings("unchecked")
    public QueryResult explainPropertyValue(OWLIndividual s, OWLProperty p, OWLObject o) throws OWLException {
        if (p.isOWLObjectProperty()) {
            return explainAxiom(OWL.propertyAssertion(s, (OWLObjectProperty) p, (OWLIndividual) o));
        } else {
            return explainAxiom(OWL.propertyAssertion(s, (OWLDataProperty) p, (OWLLiteral) o));
        }
    }

    /**
     * Computes a "subclass of" query (subclass).
     *
     * @param sub the subclass of the "subclass of" axiom
     * @param sup the super class of the "subclass of" axiom
     * @return the result of the computation contained in a QueryResult object
     * @throws OWLException
     */
    private QueryResult explainSubClass(OWLClass sub, OWLClass sup) throws OWLException {

        if (sub.equals(sup)) {
            return new QueryResult();
        }

        if (sub.isOWLNothing()) {
            return new QueryResult();
        }

        if (sup.isOWLThing()) {
            return new QueryResult();
        }

        OWLSubClassOfAxiom axiom = OWL.subClassOf(sub, sup);
        return explainAxiom(axiom);

    }

    /**
     * Computes an "instance of" query (instance).
     *
     * @param owlIndividual the individual of the class assertion axiom
     * @param owlClass the class of the class assertion axiom
     * @return the result of the computation contained in a QueryResult object
     * @throws OWLException
     */
    private QueryResult explainInstance(OWLIndividual owlIndividual, OWLClass owlClass) throws OWLException {
        if (owlClass.isOWLThing()) {
            return new QueryResult();
        }

        OWLAxiom axiom = OWL.classAssertion(owlIndividual, owlClass);

        return explainAxiom(axiom);
    }

    /**
     * Computes an "equivalent of " query. NOTE: a equivalent-of b corresponds
     * to a subclass-of b AND b subclass-of a
     *
     * @param c1 the first class of the equivalent classes axiom
     * @param c2 the second class of the equivalent classes axiom
     * @return the result of the computation contained in a QueryResult object
     * @throws OWLException
     */
    public QueryResult explainEquivalentClass(OWLClass c1, OWLClass c2) throws OWLException {
        if (c1.equals(c2)) {
            return new QueryResult();
        }

        OWLAxiom axiom = OWL.equivalentClasses(c1, c2);

        return explainAxiom(axiom);
    }

    /**
     * Computes the explanations of a generic given axiom.
     *
     * @param axiom the axiom to compute the set of explanations
     * @return the result of the computation contained in a QueryResult object
     * @throws OWLException
     */
    private QueryResult explainAxiom(OWLAxiom axiom) throws OWLException {

        MultipleExplanationGenerator expGen = new BundleHSTExplanationGenerator(getSingleExplanationGenerator());
        //MultipleExplanationGenerator expGen = new HSTExplanationGenerator(getSingleExplanationGenerator());
        BundleRendererExplanationProgressMonitor rendererMonitor;
        if (log) {
            rendererMonitor = new LogRendererTimeExplanationProgressMonitor(axiom);
        } else {
            rendererMonitor = new ScreenRendererTimeExplanationProgressMonitor(axiom);
        }
        expGen.setProgressMonitor(rendererMonitor);

        OWLClassExpression unsatClass = converter.convert(axiom);

        QueryResult result = new QueryResult();

        Timer timer = timers.startTimer("explain");
        rendererMonitor.setParamAndStart(getMaxTime());

        Set<Set<OWLAxiom>> explanations = expGen.getExplanations(unsatClass, getMaxExplanations());

        rendererMonitor.stopMonitoring();
        timer.stop();

        if (explanations.isEmpty()) {
            rendererMonitor.foundNoExplanations();
        }

        int expSize = explanations.size();
        if (expSize == 0) {
            errorExpCount++;
        } else if (expSize == 1) {
            if (explanations.iterator().next().size() > 1) {
                multiAxiomExpCount++;
            }
        } else {
            multipleExpCount++;
        }

        result.setExplanations(explanations);

        return result;
    }

    /**
     * Creates a PMap from the probabilistic ontology.
     *
     * @throws NumberFormatException
     */
    private void setPMap() throws NumberFormatException {

        if (PMap != null) {
            return;
        }

        Timer PMapTimer = timers.startTimer("PMap");
        OWLOntology ontologyprob;
        OWLOntologyManager managerprob = null;
        //if (getInputFiles().length == 2) {
//            loaderprob = (OWLAPILoader) getLoader("OWLAPIv3");
//            getKB(loaderprob, 1);
//            ontologyprob = loaderprob.getOntology();
        if (probOntologyName != null) {
            managerprob = OWLManager.createOWLOntologyManager();
            try {
                ontologyprob = managerprob.loadOntologyFromOntologyDocument(IRI.create(probOntologyName));
            } catch (OWLOntologyCreationException ex) {
                // TODO
                ontologyprob = baseOntology;
            }
        } else {
            ontologyprob = baseOntology;
        }

        SortedSet<OWLAxiom> axioms = new TreeSet<>(ontologyprob.getAxioms());
        PMap = BundleUtilities.createPMap(axioms, true, false, false, accuracy, null);

        if (probOntologyName != null) {
            managerprob.removeOntology(ontologyprob);
        }
        ontologyprob = null;

        PMapTimer.stop();

    }

    /**
     * Set the PMap as the given one.
     *
     * @param PMap the PMap to set
     */
    public void setPMap(Map<OWLAxiom, BigDecimal> PMap) {
        this.PMap = PMap;
    }

    /**
     * Computes the probability of a set of explanations contained in a
     * QueryResult object. After the computation of the probability, the given
     * QueryResult object will update
     *
     * @param explanations a QueryResult object containing the set of
     * explanations of which the method will compute the probability
     */
    private void computeProbability(QueryResult explanations) {
        if (findProbability && PMap != null) {
            Timer timerBDD = timers.startTimer("BDDCalc");

            // VarAxAnn composed by two (three) arrays:
            //      prob    (probability of axioms)
            //      axi     (OWLAxiom)
            //     (axp     (translations of axioms into strings))
            setBddF();

            BDD bdd = getBDD(explanations.getExplanations());

            BigDecimal pq = probabilityOfBDD(bdd, new HashMap<BDD, BigDecimal>());

            timerBDD.stop();

            if ((pq.compareTo(new BigDecimal("1.0")) < 0)
                    && (//BlockedIndividualSet.isApproximate() || 
                    getMaxExplanations() == explanations.getNumberOfExplanations())) {
                if (log) {
                    logger.warn("WARNING! The value of the probability may be a lower bound.");
                } else {
                    System.out.println("WARNING! The value of the probability may be a lower bound.");
                }
            }

            explanations.setBDD(bdd);
            explanations.setQueryProbability(pq);
        } else {
            setBddF();
            explanations.setBDD(getBddF().one());
            explanations.setQueryProbability(BigDecimal.ONE);
        }
        explanations.setProbAxioms(Arrays.asList(axi));
        explanations.setProbOfAxioms(Arrays.asList(prob));
    }

    /**
     * Build the BDD corresponding to the set of explanations given
     *
     * @param explanations the set of explanations for which the method
     * calculates the corresponding BDD
     * @return the resulting BDD
     */
    private BDD getBDD(Set<Set<OWLAxiom>> explanations) {
        BDD bdd = getBddF().zero();

        //for all explanation (minA) in explanations (CS)
        for (Iterator<Set<OWLAxiom>> it = explanations.iterator(); it.hasNext();) {
            BDD bdde = getBddF().one();
            Set<OWLAxiom> s = it.next();
            //for all axiom in explanation
            for (Iterator<OWLAxiom> it1 = s.iterator(); it1.hasNext();) {
                boolean stop = false;
                BDD bdda;
                OWLAxiom s1 = it1.next();
                String s1name = BundleUtilities.getManchesterSyntaxString(s1);
                //System.out.println(stringWriter.toString());

                // TO CHECK AND FIX
                if (s1name.startsWith("Transitive")) {
                    String[] splits1name = s1name.split(" ", 2);
                    s1name = splits1name[1] + " type " + splits1name[0] + "Property";
                }

                if (!PMap.containsKey(s1)) {
                    bdda = getBddF().one();
                } else {
                    // Creazione e inizializzazione di Res
                    BigDecimal probAx = PMap.get(s1);

                    bdda = getBddF().zero();

                    if (stop) {
                        bdde.andWith(bdda);
                        //printCalculateBDD(s, s1, key, axiomVar);
                        break;
                    }

                    int i;
                    boolean found = false;
                    for (i = 0; i < axi.length; i++) {
                        if (axi[i].equalsIgnoreAnnotations(s1)) {
                            found = true;
                            break;
                        }
                    }

                    //for (Double ithProb : probsInstSet) {
                    if (!found) {
                        int oldLength = axi.length;
                        String[] temp = new String[oldLength + 1];  // The new array.
                        BigDecimal[] tempd = new BigDecimal[oldLength + 1];
                        OWLAxiom[] tempai = new OWLAxiom[oldLength + 1];
                        System.arraycopy(axp, 0, temp, 0, oldLength);
                        System.arraycopy(prob, 0, tempd, 0, oldLength);
                        System.arraycopy(axi, 0, tempai, 0, oldLength);
                        axp = temp;  // Set playerList to refer to new array.
                        prob = tempd;
                        axi = tempai;
                        prob[oldLength] = probAx;
                        axp[oldLength] = s1name;
                        axi[oldLength] = s1;
                        if (i >= getBddF().varNum()) {
                            getBddF().setVarNum(getBddF().varNum() + 1);
                        }
                    }
//                  
                    BDD b = getBddF().ithVar(i);
                    bdda.orWith(b);

//                    getBddF().setVarNum(getBddF().varNum() + probsInstSet.size());
//                    for (int iForProbs = 0; iForProbs < probsInstSet.size(); iForProbs++) {
//                        BDD b = getBddF().ithVar(i + iForProbs);
//                        bdda.orWith(b);
//                    }
                }

                bdde.andWith(bdda);
            }

            bdd.orWith(bdde);
        }
        return bdd;
    }

    /**
     * Computes the probability of the given BDD. This is a recursive method
     * that takes as input a BDD (that is the current node) and a map between
     * all the already computed nodes and the corresponding probabilities in
     * order to avoid to recompute the probability of already visited nodes
     *
     * @param node the current BDD
     * @param nodes all the already computed probability with the corresponding
     * BDD
     * @return
     */
    public BigDecimal probabilityOfBDD(BDD node, Map<BDD, BigDecimal> nodes) {

        if (node.isOne()) {
            return MathUtilities.getBigDecimal("1", accuracy);
        } else if (node.isZero()) {
            return MathUtilities.getBigDecimal("0", accuracy);
        } else {

            BigDecimal value_p = getValue(node, nodes);
            if (value_p != null) {
                return value_p;
            } else {
                BigDecimal p = prob[node.var()];
                BigDecimal notP = MathUtilities.getBigDecimal("1", accuracy).subtract(p);
                BigDecimal pt = p.multiply(probabilityOfBDD(node.high(), nodes));
                pt = pt.add(notP.multiply(probabilityOfBDD(node.low(), nodes)));
                pt = pt.setScale(accuracy, RoundingMode.HALF_UP);
                add_node(node, pt, nodes);
                return pt;
            }

        }

        //return 0.0;
    }

    /**
     * Looks for the given BDD in the map between already visited BDDs an
     * probabilities
     *
     * @param node the current BDD
     * @param nodes all the already computed probability with the corresponding
     * BDD
     * @return null if not yet visited or the probability previously computed
     */
    private BigDecimal getValue(BDD node, Map<BDD, BigDecimal> nodes) {
        if (nodes.containsKey(node)) {
            return nodes.get(node);
        } else {
            return null;
        }
    }

    /**
     * Adds to the map of the already visited node a new node.
     *
     * @param node the new node (BDD) to be added
     * @param pt the probability of node
     * @param nodes all the already computed probability with the corresponding
     * BDD
     */
    private void add_node(BDD node, BigDecimal pt, Map<BDD, BigDecimal> nodes) {
        nodes.put(node, pt);
    }

    /**
     * **********************************************************************
     */
//    private void printCalculateBDD(Set<OWLAxiom> exp, OWLAxiom ax, String probType, String axType) {
//        /*
//         System.out.println("\n\nSPIEGAZIONE ANNULLATA!\n");
//         for (OWLAxiom axiom : exp){
//            
//         String s1name = Utilities.getManchesterSyntaxString(axiom);
//         System.out.println("    " + s1name);
//         if (axiom.getAxiomType().toString().equals("SubClassOf") ||
//         axiom.getAxiomType().toString().equals("TransitiveObjectProperty") ||
//         axiom.getAxiomType().toString().equals("SubObjectPropertyOf") ||
//         axiom.getAxiomType().toString().equals("SubPropertyChain") ||
//         axiom.getAxiomType().toString().equals("SubDataPropertyOf")){
//         System.out.println("        "+ ((OWLIndAxiom)axiom).getSubjName());
//         if (((OWLIndAxiom)axiom).equalsStrict(ax)){
//         System.out.println("     <= CAUSATO DA QUESTO ASSIOMA\n"+
//         "           Prob\t :  Assioma\n" +
//         "           " + probType + "\t :    " + axType);
//         }
//         } else if (axiom.equals(ax)){
//         System.out.println("     <= CAUSATO DA QUESTO ASSIOMA\n"+
//         "           Prob\t :  Assioma\n" +
//         "           " + probType + "\t :    " + axType);
//         }
//            
//         }
//         */
//        throw new UnsupportedOperationException("Useless method.");
//    }
    @Override
    public void run() {
        throw new UnsupportedOperationException("Useless method."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the maxExplanations
     */
    public int getMaxExplanations() {
        return maxExplanations;
    }

    /**
     * @param maxExplanations the maxExplanations to set
     */
    public void setMaxExplanations(int maxExplanations) {
        this.maxExplanations = maxExplanations;
    }

    /**
     * @return the maximum time for execution
     */
    public int getMaxTime() {
        return maxTime;
    }

    /**
     * @param maxTime the maximum time for execution to set (in milliseconds)
     */
    public void setMaxTime(int maxTime) {
        this.maxTime = maxTime;
    }

    /**
     * @param maxTime the string of the maximum time for execution to set
     */
    public void setMaxTime(String maxTime) {
        this.maxTime = BundleUtilities.convertTimeValue(maxTime);
    }

    /**
     * @return the bdd factory
     */
    public BDDFactory getBddF() {
        return bddF;
    }

    /**
     * @param bddF the bdd factory to set
     */
    public void setBddF(BDDFactory bddF) {
        this.bddF = bddF;
    }

    /**
     * Initializes the BDD factory.
     */
    public void setBddF() {
        if (this.bddF == null) {
            // TODO USE public static BDDFactory init(String bddpackage,int nodenum,int cachesize)
            // old version this.bddF = BuDDyFactory.init(100, 10000);
            this.bddF = BDDFactory.init(bddFType,100, 10000);
        }
    }

    /**
     * @param probOntology the probOntology to set
     */
    public void setProbOntologyName(String probOntology) {
        this.probOntologyName = probOntology;
    }

    /**
     * Print if verbose mode.
     *
     * @param msg the message to be written
     * @param log true if writing on log is enabled, false otherwise
     */
    protected void verbose(String msg, boolean log) {
        if (verbose) {
            if (log) {
                logger.info(msg);
            }
            System.err.println(msg);
        }

    }

    /**
     * Print in verbose mode.
     *
     * @param msg the message to be written
     */
    @Override
    protected void verbose(String msg) {
        verbose(msg, log);
    }

    /**
     * Print in standard mode.
     *
     * @param msg the message to be written
     * @param log true if writing on log is enabled, false otherwise
     */
    protected void output(String msg, boolean log) {
        if (log) {
            logger.info(msg);
        } else {
            System.out.println(msg);
        }
    }

    /**
     * Print in standard mode.
     *
     * @param msg the message to be written
     */
    @Override
    protected void output(String msg) {
        if (log) {
            output(msg, true);
        }
        output(msg, false);
    }

    /**
     * Prints final results.
     *
     * @param result
     */
    public void printResult(QueryResult result) {
        output("");
        output("============ Result ============");
        output("N. of Explanations: " + result.getNumberOfExplanations());
        if (findProbability) {
            verbose("Probabilistic axioms used:", log);

            for (int i = 0; i < result.getProbAxioms().size(); i++) {
                verbose("\t" + result.getProbAxioms().get(i) + " "
                        + result.getProbOfAxioms().get(i), log);
            }
            output("");
            output("Probability of the query: " + result.getQueryProbability());
        }

        output("");
        output("================================");
        output("");

    }

    /**
     * @param log the log to set
     */
    public void setLog(boolean log) {
        this.log = log;
    }

    /**
     * @return the accuracy
     */
    public int getAccuracy() {
        return accuracy;
    }

    /**
     * @param accuracy the accuracy to set
     */
    public void setAccuracy(int accuracy) {
        this.accuracy = accuracy;
    }

    /**
     * @param out A PrintStream for redirecting stdout
     */
    public void setOut(PrintStream out) {
        System.setOut(out);
    }
    
    /**
     * @param out A PrintStream for redirecting stderr
     */
    public void setErr(PrintStream out) {
        System.setErr(out);
    }
    
    
    public String getBddFType() {
        return bddFType;
    }

    public void setBddFType(String bddFType) {
        this.bddFType = bddFType;
    }
    

    public void setFindProbability(boolean findProbability) {
        this.findProbability = findProbability;
    }
    
    public void disposeBDDFactory() {
        bddF.done();
    }
}
